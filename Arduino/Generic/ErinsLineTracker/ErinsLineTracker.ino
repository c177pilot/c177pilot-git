//Define the input and output pins from the microcontroller
#define motorLeft 3
#define motorRight 9

#define lineLeftPin A0
#define lineCenterPin A1
#define lineRightPin A2

#define rightSensorCal 0 //214
#define leftSensorCal 0 //190
#define centerSensorCal 0 //236

//Analog ranges are from 0 to 255, where 255 is full speed
#define FASTSPEED 255
#define SLOWSPEED 0


void setup() {
  Serial.begin(9600);
//  pinMode(A0,INPUT);
//  pinMode(A1,INPUT);
//  pinMode(A2,INPUT);
//  pinMode(motor1Pin,OUTPUT);
//  pinMode(motor2Pin,OUTPUT);
  analogReference(DEFAULT);
}

void loop() {
  int threshold = 920;
  int lineLeft = lineLeftSensor ();
  int lineCenter =lineCenterSensor ();
  int lineRight = lineRightSensor ();

  Serial.println("-L-   -C-   -R-"); 
  Serial.print(lineLeft);
  Serial.print("   ");
  Serial.print(lineCenter);
  Serial.print("   ");
  Serial.println(lineRight);
  
  if ( lineCenter > threshold ) 
  {
    moveForward();
  }
  else{
    if ( lineLeft > threshold ) 
    {
      moveLeft();
    }
    if (lineRight > threshold)
    {
      moveRight();
    }
  }
  
  delay(10);

}

int lineRightSensor() {
/* Add your code here to make the robot measure the brightness under the right sensor */
  return analogRead(lineRightPin) - rightSensorCal;
}

int lineLeftSensor() {
/* Add your code here to make the robot measure the brightness under the left sensor */
  return analogRead (lineLeftPin) - leftSensorCal;
}

int lineCenterSensor() {
/* Add your code here to make the robot measure the brightness under the center sensor */
  return analogRead (lineCenterPin) - centerSensorCal;
}

void moveLeft() {
/* Add your code here to make the robot turn left when this function is used */
  analogWrite (motorLeft, FASTSPEED) ;
  analogWrite (motorRight, SLOWSPEED) ;
}

void moveRight() {
/* Add your code here to make the robot turn right when this function is used */
   analogWrite (motorRight, FASTSPEED) ;
   analogWrite (motorLeft, SLOWSPEED) ;
}

void moveForward() {
/* Add your code here to make the robot turn right when this function is used */
   analogWrite (motorRight, FASTSPEED) ;
   analogWrite (motorLeft, FASTSPEED) ;
}
