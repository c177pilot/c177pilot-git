/*

Various input types demonstration
one analog (potentiometer), one button, switch, and encoder
XPlaneData Library used

Download XPlaneData Library on the site.

  by Vlad Sychev 2014
http://arduino.svglobe.com
 */

//***** library ******************************************************************************

#include <SPI.h>         
#include <Ethernet.h>
#include <XPlaneData.h>         // -- include XPlaneData library
//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8, 0};                     // X-Plane IP address ( ! set your !)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)

//--------------------------------------- Analog Inputs-----------------------------


//----------------------
void setup()
      {
   XP.begin();      //==! init XPlaneData 
   
   XP.Pins_In (0, 13);   //Define pins for inputs (first, last including) 
 
 XP.menu_in ("1800");    // initiate the "Forward view"

  }
//--------------------------
void loop() 

{
  //------------Analog inputs using------------
  
 // ----Analog input pin A0 -------------
 
XP.Analog_Input(0, "cockpit2/controls/flap_ratio[0]", 0, 1, 5);     // Flaps, 5 positions precision


 //============================ Digital inputs section ===================================================


  // Pin## 4, 10, 13 not used
                                                       
// Button - momentary action (toggle commands)

XP.Button_Cmd(3, "radios/com1_standy_flip");                  //--  Com1/Com1-stby flip
 

 // ---- Toggle switches                 

XP.Switch_Com (5, "systems/avionics_off", "systems/avionics_on");      // Master Avionics switch    


//=================================== Encoders Section ==================

// -----! Pin #8-9  - Encoder for Com1
 
XP.Encoder_Com(8, "radios/stby_com1_coarse_down", "radios/stby_com1_coarse_up",  0);        
         
 }  


