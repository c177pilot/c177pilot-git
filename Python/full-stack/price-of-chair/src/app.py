__author__ = 'crose'

import requests
from bs4 import BeautifulSoup

request = requests.get("http://www.johnlewis.com/herman-miller-sayl-office-chairs/p2043346?colour=Aristo")
#<span itemprop="price" class="now-price">£615.00</span> #This is what we are looking for
content = request.content
soup = BeautifulSoup(content,"html.parser")


element = soup.find("span", {"itemprop":"price","class":"now-price"})

print (element.text.strip())
