# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 19:36:35 2016

@author: crose
It computes the far-field point source radiation pattern (array factor) for a
three-dimensional array.  The point sources can be anywhere in 3-D space.
The locations of the point sources are specified by the x,y,z elemenents of the 
AUT structure. (i.e. AUT.x, AUT.y, AUT.z) where an assumed unit definition is implied.
AUT.lam is the operating wavelength of the array given in the same unit definition as
the AUT.x, AUT.y, and AUT.z values.
The operating wavelength is part of the structure as AUT.lam;
The element amplitude and phase distributions are given by AUT.amp, AUT.phase
respectively, and are in linear amplitude and degrees phase.
The array factor is computed in space for locations given by meas.theta, and meas.phi.
Where meas.theta, meas.phi are in degrees in the standard spherical coordinate system.
Phi is the angle in the x-y plane as measured from the x-axis towards y+
Theta is the angle from the z-axis towards the x-y plane.
'y' is returned with the 'theta' corresponding to row indices, and
    'phi' values corresponding to column indices.

"""

import numpy as np
import cmath as cm

class array:
    """Phased Array simulation"""
    def __init__(self, el_amp, el_phs, el_x, el_y, el_z, el_lam):
        """Initializes array parameters"""
        self.el_amp = el_amp
        self.el_phs = el_phs
        self.el_x = el_x
        self.el_y = el_y
        self.el_z = el_z
        self.el_lam = el_lam
        
    def array_factor(self,theta_rng,phi_rng,res):
        theta_vec = np.deg2rad(np.linspace(-theta_rng, theta_rng, theta_rng/res)) 
        phi_vec = np.deg2rad(np.linspace(-phi_rng, phi_rng, phi_rng/res))

        #self._z = np.zeros((self.res, self.res))
        # self._mask = self._z != 0
        theta_space, phi_space = np.meshgrid(theta_vec, phi_vec)
#        for xpos, ypos, zpos, amp, phs in zip(self.el_x, self.el_y, self.el_z, self.el_amp, self.el_phs):
#            print xpos
#            print ypos
#            print zpos
#            print "-------------------"
        for theta in theta_vec:
            for phi in phi_vec:
                rx = np.cos(phi)*np.sin(theta)
                ry = np.sin(phi)*np.sin(theta)
                rz = np.cos(theta)
                
                XdotR = self.el_x*rx + self.el_y*ry + self.el_z*rz
                print XdotR
                temp1 = np.dot(1j,np.deg2rad(self.el_phs))
                temp2 = np.dot(self.el_amp,np.exp(temp1))
                temp3 = np.dot(1j,2*np.pi*XdotR)
                y = np.sum(np.sum(np.dot(temp2,temp3)))
                print y
            
params_trkfeed = {    \
'el_amp' : [10,10,10],  \
'el_phs' : [5,5,5],   \
'el_x' : [-1,0,1],   \
'el_y' : [0,0,0], \
'el_z' : [0,0,0], \
'el_lam' : 0.25 } #np.linspace(1,1,10)}

TrkFeed = array(**params_trkfeed)
TrkFeed.array_factor(10,20,1)