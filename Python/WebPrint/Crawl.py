# -*- coding: utf-8 -*-
"""
Created on Sun Sep 25 08:56:08 2016

@author: chris
"""

import httplib2
from BeautifulSoup import BeautifulSoup, SoupStrainer

http = httplib2.Http()
status, response = http.request('http://manuals.sierraskyport.com/pdf/AMM/SR22/html/ammmain.html')

for link in BeautifulSoup(response, parseOnlyThese=SoupStrainer('a')):
    if link.has_attr('href'):
        print link['href']