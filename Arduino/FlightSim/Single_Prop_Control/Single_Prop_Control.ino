/*
base code template for one-engine piston airplane.
(as Cessna-172, Cessna-182. etc) using XPlaneData library
for X-Plane Simulator 

Download XPlaneData Library on the site.

  by Vlad Sychev 2014
http://arduino.svglobe.com
 */

//***** library ******************************************************************************

#include <SPI.h>         
#include <Ethernet.h>
#include <XPlaneData.h>         // -- include XPlaneData library
//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8};                     // X-Plane IP address ( ! set your !)
//IPAddress ip;                                   // Arduino IP address (Auto)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)

//--------------------------------------- Analog Inputs-----------------------------

int adf; // ADF encoder built-in button status (cyclic  3-digits positions ) 

void setup()
      {
   XP.begin();      //==!== init XPlaneData
 
   XP.Pins_In (0, 49);   //Define pins for outputs (first, last including)   

  }
     

//***************** Main program loop *************************************

void loop() 
{
 // ------------------------- 3-Axis (transmitting is limited to 33  per second)-------------

  
XP.Analog_Input(0, "cockpit2/controls/yoke_pitch_ratio[0]", -1, 1, 500);  // Pitch yoke, precision 500
XP.Analog_Input(1, "cockpit2/controls/yoke_roll_ratio[0]", -1, 1, 500);    // Roll  yoke, precision 500
XP.Analog_Input(2, "cockpit2/controls/yoke_heading_ratio[0]", -1, 1, 500);  // Rudder, precision 1000

 
//=== Throttle (DREF packet )  Value is transmitted only if it was changed at more than Dead_zone ) --------- 

XP.Analog_Input(3, "cockpit2/engine/actuators/throttle_ratio[0]", 0, 1, 300);  // Throttle, precision 300


//=========================  Prop Pitch Angle ( DREF packet ) for Cessna 182 etc. (2 variants) ---------

// ! This is for the "Manual Pitch" planes (See the PlaneMaker --> Engine Specs --> Location tab --> 2nd parameter ----
//      - aircraft/prop/acf_prop_type = 2   (0=fixed, 1=const.RPM, 2=manual) 
//       We can read this dataref from X-Plane to choose which code to use (next version):

XP.Analog_Input(4, "cockpit2/engine/actuators/prop_pitch_deg[0]", 0, 1, 300);  // 
//XP.Analog_Input(4, "cockpit2/engine/actuators/prop_rotation_speed_rad_sec[0]", 0, 1, 300);  // 

 //=======================================-  Mixture ( DREF packet ) ---------

XP.Analog_Input(5, "cockpit2/engine/actuators/mixture_ratio[0]", 0, 1, 300);  // 


//==================================  Flaps ( DREF packet ) ================================

//-------------------- Connect to analog input potentiometer 10 kOm - and make lever with several fixed positions (if needed)
//---------------------( must  be the full stroke of potentiometer, use the gears ) 

XP.Analog_Input(6, "cockpit2/controls/flap_ratio[0]", 0, 1, 5);     // Flaps, 5 positions precision
        
             
//=======================================-  Starter/Magnetos =======================================

// !-- Connect to analog input the set of resistors -GND-----1k-----1k-----1k-----1k-----0.36k----+5v
//--------------------------------------------------------^------^------^------^------^-------------- 
//------------------------------------------------------ 5 positions, from the last position the knob returns to previous by spring
//------- Or, It could be  5-10 kOHm potentiometer, which axis is fixed on the axis of rotary toggle (then the program values must be adjusted)

 if (XP.RotarySwitch(7, 5)) {
      XP.Button_Cmd(1, "magnetos/magnetos_off_1");              //   
      XP.Button_Cmd(2, "magnetos/magnetos_right_1");            //
      XP.Button_Cmd(3, "magnetos/magnetos_left_1");             //
      XP.Button_Cmd(4, "magnetos/magnetos_both_1");             //  
      XP.Button_Rep(5, "starters/engage_starter_1");            //
 }

//=======================================-  Transponder Mode Switch ===================================

// !-- Connect to analog input the set of resistors -GND-----1k-----1k-----1k-----1k------1k------1k----+5v
//--------------------------------------------------------^------^------^------^------^-------^-----------
//------------------------ 6 positions  Transponder Mode ------------------------------

 if (XP.RotarySwitch(8, 6)) {
      XP.Button_Cmd(1, "transponder/transponder_off");      // - OFF               
      XP.Button_Cmd(2, "transponder/transponder_standby");  // - STBY 
      XP.Button_Cmd(3, "transponder/transponder_alt");      // - ALT mode
      XP.Button_Cmd(4, "transponder/transponder_test");     // - TEST mode
      XP.Button_Rep(5, "transponder/transponder_on");       // - ON mode
      XP.Button_Rep(6, "transponder/transponder_ground");   // - GND mode      
 }
      
//=======================  Panel/Radio Lighting Brightness ( DREF packet ) =============================

XP.Analog_Input(6, "cockpit2/switches/instrument_brightness_ratio[0]", 0, 1, 100);  
XP.Analog_Input(6, "cockpit2/switches/instrument_brightness_ratio[1]", 0, 1, 100);

//============================ Digital inputs section ===================================================


 // ---- Toogles and Buttons

XP.Switch_Com (13, "systems/avionics_on", "systems/avionics_off");   /* Master Avionics switch  (active "1") Pin#13 must be connected 
                                                                                  to the GND through resistor 5-10k ("pulldown" resistor)
                                                                                  active toggle must connect Pin #13 to the +5v 
                                                                                   */                    
            // all other buttons/toggles connects pins to the GND (active state - "0"), no resistors are used:
                                
XP.Switch_Com (12, "electrical/generator_1_off", "electrical/generator_1_on");      // Alternator  switch  
XP.Switch_Com (11, "electrical/battery_1_on", "electrical/battery_1_off");         // BATTERY switch 
XP.Switch_Com (9, "lights/beacon_lights_on", "lights/beacon_lights_off");         // Beacon Light switch 
XP.Switch_Com (8, "lights/strobe_lights_on", "lights/strobe_lights_off");        // Strobe Light switch    
XP.Switch_Com (7, "lights/nav_lights_on", "lights/nav_lights_off");             // NAV light  switch       
XP.Switch_Com (6, "lights/taxi_lights_on", "lights/taxi_lights_off");          // Taxi  light switch        
XP.Switch_Dref (5, "cockpit2/switches/landing_lights_on[0]", 0, 1);               //-- Landing Lights  ---
XP.Switch_Com (3, "fuel/fuel_pump_1_on", "fuel/fuel_pump_1_off");            // FUEL Pump
XP.Switch_Com (2, "ice/pitot_heat0_on", "ice/pitot_heat0_off");             // Pitot Heat switch 

XP.Button_Cmd (1, "flight_controls/landing_gear_up");                        //---- GEAR UP  
XP.Button_Cmd (0, "flight_controls/landing_gear_down");                     //---- GEAR DN         

//============ Audio selector Panel (7 buttons on the  pins #14-20 ) - for X-Plane 10 only

XP.Button_Cmd (14, "audio_panel/monitor_audio_com1");        //  A_Com1
XP.Button_Cmd (15, "audio_panel/monitor_audio_com2");        //  A_Com2    
XP.Button_Cmd (16, "audio_panel/monitor_audio_nav1");        //  A_Nav1
XP.Button_Cmd (17, "audio_panel/monitor_audio_nav2");        //  A_Nav2     
XP.Button_Cmd (18, "audio_panel/monitor_audio_adf1");        //  A_Adf 1
XP.Button_Cmd (19, "audio_panel/monitor_audio_dme");         //  A_DME  
XP.Button_Cmd (20, "audio_panel/monitor_audio_mkr");         //  A_Mkr 
 
XP.Button_Cmd (21, "transponder/transponder_ident");         //------Transponder IDENT button ----  
      
XP.Button_Cmd(22, "radios/com1_standy_flip");                  //--  Com1/Com1-stby flip
XP.Button_Cmd(23, "radios/nav1_standy_flip");                  //--- NAV1/NAV1-stby flip
XP.Button_Cmd(24, "radios/com2_standy_flip");                  //--- Com2/Com2-stby flip
XP.Button_Cmd(25, "radios/nav2_standy_flip");                  //--- NAV2/NAV2-stby flip
XP.Button_Cmd(26, "radios/adf1_standy_flip");                  //--- Change ADF stdby Freq 


 if (XP.D_read(27) == 1) {adf++; if (adf == 3) adf = 0; delay(15);}   
 
    //--- Encoders button PUSH/OFF
  //for  ADF: push one = "1"; push two = "10", push three = "100"
                                                                                      

//=================================== Encoders Section ==================

//------! encoders use paired inputs - 28-29,  30-31,  32-33,  34-35... ... 48-49, common pin is connected to "GND"

// -----!- Encoders for Com/Nav have the built-in  Push Button (If Pressed - fine up/dn) - Pin #27
 

XP.Encoder_2xCom(28, 27, "radios/stby_com1_fine_up", "radios/stby_com1_fine_down", "radios/stby_com1_coarse_up", "radios/stby_com1_coarse_down", 0);        // Com1 Freq   
XP.Encoder_2xCom(30, 27, "radios/stby_com2_fine_up", "radios/stby_com2_fine_down", "radios/stby_com2_coarse_up", "radios/stby_com2_coarse_down", 0);         // Com2 Freq
XP.Encoder_2xCom(32, 27, "radios/stby_nav1_fine_up", "radios/stby_nav1_fine_down", "radios/stby_nav1_coarse_up", "radios/stby_nav1_coarse_down", 0);         // Nav1 Freq
XP.Encoder_2xCom(34, 27, "radios/stby_nav2_fine_up", "radios/stby_nav2_fine_down", "radios/stby_nav2_coarse_up", "radios/stby_nav2_coarse_down", 0);         // Nav2 Freq


if(adf==0) XP.Encoder_Com(36, "radios/stby_adf1_ones_up", "radios/stby_adf1_ones_down", 0);          // ADF Freq 1
if(adf==1) XP.Encoder_Com(36, "radios/stby_adf1_tens_up", "radios/stby_adf1_tens_down", 0);          // ADF Freq 10
if(adf==2) XP.Encoder_Com(36, "radios/stby_adf1_hundreds_up", "radios/stby_adf1_hundreds_down", 0);  // ADF Freq 100

XP.Encoder_Com(38, "radios/obs2_up", "radios/obs2_down", 0);                        // Nav2 OBS 
XP.Encoder_Com(40, "instruments/barometer_up", "instruments/barometer_down", 0); // Barometer, alt  

//---------------- Transponder code (4 encoders on the pins #42-43, 44-45, 46-47, 48-49

XP.Encoder_Com(42, "transponder/transponder_ones_up", "transponder/transponder_ones_down", 0);               // Transponder 1
XP.Encoder_Com(44, "transponder/transponder_tens_up", "transponder/transponder_tens_down", 0);                // Transponder 10   
XP.Encoder_Com(46, "transponder/transponder_hundreds_up", "transponder/transponder_hundreds_down", 0);         // Transponder 100
XP.Encoder_Com(48, "transponder/transponder_thousands_up", "transponder/transponder_thousands_down", 0); // Transponder 1000
//---------------------------------------

 }  //=========--- end 

/*  !!!!!
Don't leave in the final code any lines with Serial.print(), that you could used for testing! 
That will take a lot of processing time, and lead to encoders timing inconsistency. 
Comment  or delete the lines with Serial.print() ! 

If you're not using some analog input, which has active code - connect it to GND, or "comment' this part of code! 
(Or it will constantly send interference data and take time too)


  by Vlad Sychev .2014
 */



