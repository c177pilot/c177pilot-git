# -*- coding: utf-8 -*-
"""
Created on Mon May 09 21:23:04 2016

@author: crose
"""

import os
import Tkinter, tkFileDialog
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cbook as cbook
import pandas as pd

#file_path = tkFileDialog.askdirectory()
file_path = 'D:/EngineLog'

for filename in os.listdir(file_path):
    if filename.endswith(".log"):
        fp = os.path.join(file_path, filename)
        print(fp)
        df = pd.DataFrame.from_csv(fp, header=2,parse_dates=True)
        try:
            df['C1'].plot()      
#            df['C2'].plot()
        except:
            pass
        else:
            pass
#        df.C1.plot(color='g',lw=1.3)
#        df.C2.plot(color='r',lw=1.3)
        
#        data = np.loadtxt(fp, delimiter=',', skiprows=3)
#        fig = plt.figure()
#        ax1 = fig.add_subplot(111)
#        ax1.set_title("Mains power stability")    
#        ax1.set_xlabel('time')
#        ax1.set_ylabel('Mains voltage')
#        
#        ax1.plot(data[1],data[12], c='r', label='CHT1')
#        
#        leg = ax1.legend()
#        
#        plt.show()        
#        print(file)