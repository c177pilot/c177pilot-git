/*******************************************
 Sparki Line-following example
 
 Threshold is the value that helps you 
 determine what's black and white. Sparki's 
 infrared reflectance sensors indicate white 
 as close to 900, and black as around 200.
 This example uses a threshold of 700 for 
 the example, but if you have a narrow line, 
 or perhaps a lighter black, you may need to 
 adjust.
********************************************/

#include <Sparki.h> // include the sparki library
enum STATES {tracking, pickup, dropoff};

void setup() 
{ sparki.gripperOpen();
  delay(2000);
}

void loop() {
  int threshold_dark = 500;
  int threshold_white = 800;
  
  int lineLeft   = sparki.lineLeft();   // create a variable called "lineLeft" and set it equal to the measured value from the left IR sensor
  int lineCenter = sparki.lineCenter(); // same as above except using the center IR sensor
  int lineRight  = sparki.lineRight();  // same as above except using the right IR sensor

  if ( lineCenter < threshold_dark ) // if line is below left line sensor
  {  
    sparki.moveForward(); // move forward
  }
  else{
    if ( lineLeft < threshold_dark ) // if line is below left line sensor
    {  
      sparki.moveLeft(); // turn left
    }
  
    if ( lineRight < threshold_dark ) // if line is below right line sensor
    {  
      sparki.moveRight(); // turn right
    }
  }

    if ( lineCenter > threshold_white ) 
    {  
      sparki.moveStop();
      sparki.gripperClose();  // open the robot's gripper
      delay(2000);           // for 1 second (1000 milliseconds)
      sparki.gripperStop();
      sparki.moveForward(2);

    }
   
  
  sparki.clearLCD(); // wipe the screen
  
  sparki.print("Line Left: "); // show left line sensor on screen
  sparki.println(lineLeft);
  
  sparki.print("Line Center: "); // show center line sensor on screen
  sparki.println(lineCenter);
  
  sparki.print("Line Right: "); // show right line sensor on screen
  sparki.println(lineRight);
  
  sparki.updateLCD(); // display all of the information written to the screen

  delay(100); // wait 0.1 seconds
}
