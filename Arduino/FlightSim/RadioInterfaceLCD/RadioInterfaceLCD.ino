#define ENCODER_DO_NOT_USE_INTERRUPTS //necessary if the dedicated interrupts are not being used.
//#define USE_LCD //this will compile the code which uses the LCD. The problem is that the entire device stops working when the LCD is included. The first attempt to print stops all functions.
#define USE_BOUNCE_SWITCHES

#include <Encoder.h>
#include <Bounce.h>
#include <Wire.h>

/*Change Log
Chris Rose Dec. 2014
Everything works as planned
ToDo: The encoder stops controlling the X-plane radios when I initialize or print to the LCD. Do not know the reason. Suspect a conflict between FlightSim.update() and the LCD libary
2015-09-07 - This is the go forward basis for the X-plane radio interface
*/

#ifdef USE_LCD
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27,16,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display
#endif

//pin mapping. keeps serial port 1 and I2C ports unused on Teensy 3.1 MCU.
const int COMFLIPFLOPSWITCHPIN=2;
const int NAVFLIPFLOPSWITCHPIN=3;
const int RADIOSELECTSWITCHPIN=4;
const int COM1MONITORSWITCHPIN=5;
const int COM2MONITORSWITCHPIN=6;
const int NAV1MONITORSWITCHPIN=7;
const int NAV2MONITORSWITCHPIN=8;
const int DMEMONITORSWITCHPIN=9;
const int MKRMONITORSWITCHPIN=10;

const int EncCom_MhzPIN_A=20;
const int EncCom_MhzPIN_B=21;
const int EncNav_MhzPIN_A=16;
const int EncNav_MhzPIN_B=17;

const int EncCom_khzPIN_A=5;
const int EncCom_khzPIN_B=6;
const int EncNav_khzPIN_A=7;
const int EncNav_khzPIN_B=8;

// X-Plane objects
#ifdef USE_LCD
FlightSimInteger NavFreq_Mhz[2], NavFreq_khz[2], ComFreq_Mhz[2], ComFreq_khz[2];
#endif
FlightSimInteger NavFreq_stdby_Mhz[2], NavFreq_stdby_khz[2], ComFreq_stdby_Mhz[2], ComFreq_stdby_khz[2];
FlightSimCommand ComFlipFlop[2], NavFlipFlop[2];

#ifdef USE_BOUNCE_SWITCHES
Bounce ComFlipFlopSwitch = Bounce(COMFLIPFLOPSWITCHPIN, 5);      // Pushbutton on pin 3, 5ms debounce
Bounce NavFlipFlopSwitch = Bounce(NAVFLIPFLOPSWITCHPIN, 5);    // Pushbutton on pin 4, 5ms debounce
Bounce ComMonitorSwitch[0] = Bounce(COM1MONITORSWITCHPIN,5);

//Bounce RadioSelect = Bounce(RADIOSELECTSWITCHPIN,5);          //select switch for NavCom1 or NavCom2
#endif

Encoder EncNav_Mhz = Encoder(EncNav_MhzPIN_A,EncNav_MhzPIN_B);// Rotary Encoder A channel pin and B channel pin
Encoder EncNav_khz = Encoder(EncNav_khzPIN_A,EncNav_khzPIN_B);
Encoder EncCom_Mhz = Encoder(EncCom_MhzPIN_A,EncCom_MhzPIN_B);
Encoder EncCom_khz = Encoder(EncCom_khzPIN_A,EncCom_khzPIN_B);

long l_NavFreq_stdby_khz[2], l_NavFreq_stdby_Mhz[2], l_NavFreq_stdby_khz_prev[2], l_NavFreq_stdby_Mhz_prev[2];
long l_ComFreq_stdby_khz[2], l_ComFreq_stdby_Mhz[2], l_ComFreq_stdby_khz_prev[2], l_ComFreq_stdby_Mhz_prev[2];

long l_navEncValue_Mhz[2]={0,0};
long l_navEncValue_khz[2]={0,0};
long l_comEncValue_Mhz[2]={0,0};
long l_comEncValue_khz[2]={0,0};

long l_navEncValue_Mhz_prev[2]={0,0};
long l_navEncValue_khz_prev[2]={0,0};
long l_comEncValue_Mhz_prev[2]={0,0};
long l_comEncValue_khz_prev[2]={0,0};

int m = 0;
  
// setup runs once, when Teensy boots.
void setup() {

#ifdef USE_LCD 
  lcd.init();                      // initialize the lcd 
  lcd.clear();
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("X-Plane Radio");
  lcd.setCursor(0,1);
  lcd.print("Chris Rose");
  lcd.setCursor(0,2);
  lcd.print("Date: 12/23/2014");
  lcd.setCursor(0,3);
  lcd.print("Version 1.0");
#endif

  // bind variables to the X-Plane datarefs
  NavFreq_stdby_Mhz[0] = XPlaneRef("sim/cockpit2/radios/actuators/nav1_standby_frequency_Mhz");
  NavFreq_stdby_Mhz[1] = XPlaneRef("sim/cockpit2/radios/actuators/nav2_standby_frequency_Mhz");
  NavFreq_stdby_khz[0] = XPlaneRef("sim/cockpit2/radios/actuators/nav1_standby_frequency_khz");
  NavFreq_stdby_khz[1] = XPlaneRef("sim/cockpit2/radios/actuators/nav2_standby_frequency_khz");

  ComFreq_stdby_Mhz[0] = XPlaneRef("sim/cockpit2/radios/actuators/com1_standby_frequency_Mhz");
  ComFreq_stdby_Mhz[1] = XPlaneRef("sim/cockpit2/radios/actuators/com2_standby_frequency_Mhz");
  ComFreq_stdby_khz[0] = XPlaneRef("sim/cockpit2/radios/actuators/com1_standby_frequency_khz");
  ComFreq_stdby_khz[1] = XPlaneRef("sim/cockpit2/radios/actuators/com2_standby_frequency_khz");

#ifdef USE_LCD  
  NavFreq_Mhz[0] = XPlaneRef("sim/cockpit2/radios/actuators/nav1_frequency_Mhz");
  NavFreq_Mhz[1] = XPlaneRef("sim/cockpit2/radios/actuators/nav2_frequency_Mhz");
  NavFreq_khz[0] = XPlaneRef("sim/cockpit2/radios/actuators/nav1_frequency_khz");
  NavFreq_khz[1] = XPlaneRef("sim/cockpit2/radios/actuators/nav2_frequency_khz");  

  ComFreq_Mhz[0] = XPlaneRef("sim/cockpit2/radios/actuators/com1_frequency_Mhz");
  ComFreq_Mhz[1] = XPlaneRef("sim/cockpit2/radios/actuators/com2_frequency_Mhz");
  ComFreq_khz[0] = XPlaneRef("sim/cockpit2/radios/actuators/com1_frequency_khz");
  ComFreq_khz[1] = XPlaneRef("sim/cockpit2/radios/actuators/com2_frequency_khz");   
#endif

  ComFlipFlop[0] = XPlaneRef("sim/radios/com1_standy_flip");
  ComFlipFlop[1] = XPlaneRef("sim/radios/com2_standy_flip");  
  NavFlipFlop[0] = XPlaneRef("sim/radios/nav1_standy_flip");
  NavFlipFlop[1] = XPlaneRef("sim/radios/nav2_standy_flip");  

  //configure digital input pins for PULLUP
  pinMode(COMFLIPFLOPSWITCHPIN, INPUT_PULLUP);  // input pullup mode allows connecting
  pinMode(NAVFLIPFLOPSWITCHPIN, INPUT_PULLUP);  // buttons and switches from the pins
  pinMode(RADIOSELECTSWITCHPIN, INPUT_PULLUP);  // to ground, and the chip provide the
  
  pinMode(EncNav_MhzPIN_A, INPUT_PULLUP);
  pinMode(EncNav_MhzPIN_B, INPUT_PULLUP);
  pinMode(EncNav_khzPIN_A, INPUT_PULLUP);
  pinMode(EncNav_khzPIN_B, INPUT_PULLUP);
  pinMode(EncCom_MhzPIN_A, INPUT_PULLUP);
  pinMode(EncCom_MhzPIN_B, INPUT_PULLUP);
  pinMode(EncCom_khzPIN_A, INPUT_PULLUP);
  pinMode(EncCom_khzPIN_B, INPUT_PULLUP);  

#ifdef USE_LCD
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ready"); //when debugging, this is the last thing displayed to the LCD. Indicates a conflict with the FlightSim.update() and the LCD
#endif

  //FlightSim.update(); //cache X-plane variables

#ifdef USE_LCD
  lcd.setCursor(0,2);
  lcd.print("Done");
#endif
  
} //end of setup


//Function to display all COM and NAV frequencies
void refreshLCD() {

#ifdef USE_LCD  
  //This routine prints all frequencies to the LCD.
  char Mhz[3], khz[2];
  
  lcd.setCursor(0,0);
  lcd.print("COM1:");
  lcd.setCursor(0,1);
  lcd.print("NAV1:");
  lcd.setCursor(0,2);
  lcd.print("COM2:");
  lcd.setCursor(0,3);
  lcd.print("NAV2:");

  //set the Active:Standby colons
  for (int k=0; k<4; k++) {
    
    lcd.setCursor(8,k);
    lcd.print(".");
    
    lcd.setCursor(12,k);
    lcd.print(":");
    
    lcd.setCursor(17,k);
    lcd.print(".");
  }


  //set the displayed frequencies  
  for (int k=0; k<2; k++) {
    
    int row = 2*k;
    
    //set COM[m] active
    itoa(ComFreq_Mhz[k],Mhz,10);
    itoa(ComFreq_khz[k],khz,10);
    lcd.setCursor(5,row);    
    lcd.print(Mhz); 
    lcd.setCursor(9,row);
    lcd.print(khz); 
    
    //set COM[m] standby
    itoa(ComFreq_stdby_Mhz[k],Mhz,10);
    itoa(ComFreq_stdby_khz[k],khz,10);
    lcd.setCursor(14,row);    
    lcd.print(Mhz); 
    lcd.setCursor(18,row);
    lcd.print(khz); 
    
    row++;
    //set NAV[m] active
    itoa(NavFreq_Mhz[k],Mhz,10);
    itoa(NavFreq_khz[k],khz,10);
    lcd.setCursor(5,row);    
    lcd.print(Mhz); 
    lcd.setCursor(9,row);
    lcd.print(khz); 
    
    //set NAV[m] standby
    itoa(NavFreq_stdby_Mhz[k],Mhz,10);
    itoa(NavFreq_stdby_khz[k],khz,10);
    lcd.setCursor(14,row);    
    lcd.print(Mhz); 
    lcd.setCursor(18,row);
    lcd.print(khz); 
    
 } 
#endif
}

void loop() {

  // normally the first step in loop() should update from X-Plane

#ifdef USE_LCD 
  refreshLCD();
#endif

  FlightSim.update(); //cache X-plane variables
  
  
  //Check radio select switch
  int RadioSelect = digitalRead(RADIOSELECTSWITCHPIN); //why is this not a DEBOUNCE object
  if (RadioSelect) {
    m=1;
  } else {
    m=0;
  }
  
  l_ComFreq_stdby_khz[m] = ComFreq_stdby_khz[m];  
  l_ComFreq_stdby_Mhz[m] = ComFreq_stdby_Mhz[m];    
  l_NavFreq_stdby_khz[m] = NavFreq_stdby_khz[m];
  l_NavFreq_stdby_Mhz[m] = NavFreq_stdby_Mhz[m];


  // read the rotary encoders
  l_comEncValue_khz[m] = EncCom_khz.read() / 2;
  l_comEncValue_Mhz[m] = EncCom_Mhz.read() / 2;
  l_navEncValue_khz[m] = EncNav_khz.read() / 2;
  l_navEncValue_Mhz[m] = EncNav_Mhz.read() / 2;
  
  l_ComFreq_stdby_Mhz[m] += (l_comEncValue_Mhz[m] - l_comEncValue_Mhz_prev[m]);
  l_ComFreq_stdby_khz[m] += (l_comEncValue_khz[m] - l_comEncValue_khz_prev[m])*25;
  l_NavFreq_stdby_Mhz[m] += (l_navEncValue_Mhz[m] - l_navEncValue_Mhz_prev[m]);
  l_NavFreq_stdby_khz[m] += (l_navEncValue_khz[m] - l_navEncValue_khz_prev[m])*5;
  
  l_comEncValue_Mhz_prev[m] = l_comEncValue_Mhz[m];
  l_comEncValue_khz_prev[m] = l_comEncValue_khz[m];
  l_navEncValue_Mhz_prev[m] = l_navEncValue_Mhz[m];
  l_navEncValue_khz_prev[m] = l_navEncValue_khz[m];

  //Handle rollover of the coarse tuning
  if (l_NavFreq_stdby_Mhz[m] > 118) {
    l_NavFreq_stdby_Mhz[m] = 108;
  }  
  if (l_NavFreq_stdby_Mhz[m] < 108) {
    l_NavFreq_stdby_Mhz[m] = 118;
  }
  if (l_ComFreq_stdby_Mhz[m] < 118) {
    l_ComFreq_stdby_Mhz[m] = 136;
  }
  if (l_ComFreq_stdby_Mhz[m] > 136) {
    l_ComFreq_stdby_Mhz[m] = 118;
  }
  
  //Handle rollover of the fine tuning
  if (l_NavFreq_stdby_khz[m] > 95) {
    l_NavFreq_stdby_khz[m] = 0;
  }
  if (l_NavFreq_stdby_khz[m] < 0) {
    l_NavFreq_stdby_khz[m] = 95;
  }
  if (l_ComFreq_stdby_khz[m] > 975) {
    l_ComFreq_stdby_khz[m] = 0;
  }
  if (l_ComFreq_stdby_khz[m] < 0) {
    l_ComFreq_stdby_khz[m] = 975;
  }

  //Update X-plane dataref's
  if (l_ComFreq_stdby_Mhz[m] != l_ComFreq_stdby_Mhz_prev[m]) {
    ComFreq_stdby_Mhz[m] = l_ComFreq_stdby_Mhz[m];
    l_ComFreq_stdby_Mhz_prev[m] = l_ComFreq_stdby_Mhz[m];
  }
  if (l_ComFreq_stdby_khz[m] != l_ComFreq_stdby_khz_prev[m]) {
    ComFreq_stdby_khz[m] = l_ComFreq_stdby_khz[m];
    l_ComFreq_stdby_khz_prev[m] = l_ComFreq_stdby_khz[m];
  }
  if (l_NavFreq_stdby_Mhz[m] != l_NavFreq_stdby_Mhz_prev[m]) {
    NavFreq_stdby_Mhz[m] = l_NavFreq_stdby_Mhz[m];
    l_NavFreq_stdby_Mhz_prev[m] = l_NavFreq_stdby_Mhz[m];
  }
  if (l_NavFreq_stdby_khz[m] != l_NavFreq_stdby_khz_prev[m]) {
    NavFreq_stdby_khz[m] = l_NavFreq_stdby_khz[m];
    l_NavFreq_stdby_khz_prev[m] = l_NavFreq_stdby_khz[m];
  }

 #ifdef USE_BOUNCE_SWITCHES
  // read the pushbuttons, and send X-Plane commands to SELECTED radio they're pressed
  NavFlipFlopSwitch.update();
  if (NavFlipFlopSwitch.fallingEdge()) {
    NavFlipFlop[m] = 1;
  } else {
    //NavFlipFlop[m] = 0;
  }
  
  ComFlipFlopSwitch.update(); //read the physical switch
  if (ComFlipFlopSwitch.fallingEdge()) {
    ComFlipFlop[m] = 1;
  } else {
    //ComFlipFlop[m] = 0;
  }
#endif

  delay(5);
}


