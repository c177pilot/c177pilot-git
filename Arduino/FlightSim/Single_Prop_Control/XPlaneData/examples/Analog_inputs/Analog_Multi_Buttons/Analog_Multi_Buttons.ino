/*
I/O interface for X-Plane Cockpit Simulator
using XPlaneData library

Rotary swith on one analog input. 


Version 1.0  aug. 2014
 by Vlad and Roman Sychev
http://arduino.svglobe.com/ardref.html
 */
 
#include <SPI.h>         
#include <Ethernet.h>
//#include <Wire.h>
#include <XPlaneData.h>         // -- include XPlaneData library
//#include <XPExt.h> 
//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                     // port to listen to get UDP packets
int XPW = 49000;                                    // port to send UDP packets
int IPx[] = {192, 168, 1, 2, 0};                      // X-Plane IP address ( ! set your !)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)
//--------------------------------------------------

//--------------------------------------------------
void setup()  { 

   XP.begin();      //==! init XPlaneData (0 - auto IP, any other - 4-th  number of  IP address)

} 
//============================================
void loop() {  

//==== Analog input as multiple buttons: 10 buttons example ===========================

 if (XP.ButtonArray(0, 10)) {  //-- initiate ( 0 -> analog input number, 10 -> number of buttons)
   
      XP.Button_Cmd (1, "instruments/timer_reset");       // Timer Reset      (single command button)  
      XP.Button_Cmd (2, "radios/nav1_standy_flip");           // NAV1/NAV1-stby flip (toogle button button)
      XP.Button_Inc (3, "radios/obs_HSI_up");                 // OBS HSI +     (increment button )         
      XP.Button_Inc (4, "radios/obs_HSI_down");               // OBS HSI -     (decrement button )  
      XP.Button_Rep (5, "annunciator/test_all_annunciators"); // test annunciators  (continuity  button )      
//      XP.Button_Cmd (6, "");           // 
      XP.Button_Key (7, "g");                                 // Send key "G' (gear toggle default)         
      XP.Button_Mnu (8, "1802");                              // Menu item: 1802 = "Forwards with Nothing view"  
      XP.Button_Chr (9, 27);                               // character code  ("ESC" - for exit from menu) 
//      XP.Button_Rep (10, ""); //    
      
     }
     
// Description:
     // A_Com( button number, "command", button_type); 
     // Button type: 
                 // Button_Cmd - toggle button or single command button
                 // Button_Inc - "inc/dec" or +/- step button (after 1 sec. of hold - repeating command)
                 // Button_Rep - "continuity"  button - send the command whyle pressed
                 // Button_Key - sends keys (as keyboard) 
                 // Button_Mnu - Menu Item ( see numbers in doc - "X-Plane/Resources/menus" folder 
                 // Button_Chr - sends char decimal code  


} //-------------------  
 


