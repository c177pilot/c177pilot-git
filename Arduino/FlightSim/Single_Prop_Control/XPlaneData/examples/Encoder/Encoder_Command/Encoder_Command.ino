/*
sample code for one encoder
using XPlaneData Library
for X-Plane

Vlad
http://arduino.svglobe.com
 */

//------------------------------------
#include <SPI.h>         
#include <Ethernet.h>
#include <Wire.h>
#include <XPlaneData.h>         // -- include XPlaneData library
//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8, 0};                     // X-Plane IP address ( ! set your !)
//IPAddress ip;                                   // Arduino IP address (Auto)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)
//------------------------------------


void setup() {
  
  XP.Pins_In (0, 12);       // Define pins for inputs (first, last including)
     
   XP.begin();      //==! init XPlaneData 
 
      }
     
//==============
void loop() 
{

       //==================two Encoders on the pins  #2-3 and #6-7  ==================


XP.Encoder_Com(2, "instruments/barometer_down", "instruments/barometer_up", 0); 

XP.Encoder_Com(6, "radios/stby_nav1_fine_down", "radios/stby_nav1_fine_up", 0);

// ------------ encoder for two commands arguments:
                            // 1 - first pin#, 
                            // 2 - command1,
                            // 3 -  command2,
                            // 4 -  encoder type (0-3) ):
                            
                            
 // Encoder with built-in button function                           
       //=========== Encoder on the pins  #6-7 with  built-in  button on the pin #10 ==================

XP.Encoder_2xCom(6, 10, "radios/stby_com2_coarse_down", "radios/stby_com2_coarse_up", 
"radios/stby_com2_fine_down", "radios/stby_com2_fine_up", 0); 
                            
                            
 
}   
