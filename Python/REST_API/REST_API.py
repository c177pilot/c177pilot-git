# -*- coding: utf-8 -*-
"""
Created on Fri Apr 15 09:17:20 2016

@author: crose

http://www.dreamsyssoft.com/python-scripting-tutorial/create-simple-rest-web-service-with-python.php
"""

#!/usr/bin/env python
import web
import xml.etree.ElementTree as ET

tree = ET.parse('C:\Users\crose\Documents\Python Scripts\REST_API\user_data.xml')
root = tree.getroot()

urls = (
    '/users', 'list_users',
    '/users/(.*)', 'get_user'
)

app = web.application(urls, globals())

class list_users:        
    def GET(self):
	output = 'users:[';
	for child in root:
                print 'child', child.tag, child.attrib
                output += str(child.attrib) + ','
	output += ']';
        return output

class get_user:
    def GET(self, user):
	for child in root:
		if child.attrib['id'] == user:
		    return str(child.attrib)

if __name__ == "__main__":
    app.run()