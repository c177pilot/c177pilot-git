//DFRobot.com
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,16,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

enum  RadioType {COM,NAV};
enum FreqType {Active,Stdby};

void setup()
{
  lcd.init();                      // initialize the lcd 
 
  // Print a message to the LCD.
  lcd.backlight();

  lcd.setCursor(0,0);
  lcd.print("COM1:");
  lcd.setCursor(0,1);
  lcd.print("NAV1:");
  lcd.setCursor(0,2);
  lcd.print("COM2:");
  lcd.setCursor(0,3);
  lcd.print("NAV2:");


  //set the COM side colons
  lcd.setCursor(12,0);
  lcd.print(":");
  lcd.setCursor(12,1);
  lcd.print(":");
  lcd.setCursor(12,2);
  lcd.print(":");
  lcd.setCursor(12,3);
  lcd.print(":");  
  
  //write the COM1 frequencies
  writeFreqLCD(0,0,0,111.00); //COM1 active
  writeFreqLCD(0,1,0,222.00); //COM1 stdby
  writeFreqLCD(1,0,0,111.10); //NAV1 active
  writeFreqLCD(1,1,0,222.10); //NAV1 stdby
  
  writeFreqLCD(0,0,1,333.00); //COM2 active
  writeFreqLCD(0,1,1,444.00); //COM2 stdby
  writeFreqLCD(1,0,1,333.20); //NAV2 active
  writeFreqLCD(1,1,1,444.20); //NAV2 stdby
  
  
  
}

void loop()
{
}

void writeFreqLCD(int radiotype, int freqtype, int radionum,  float freq){
  //char buffer[6];
  int row = 2*radionum + radiotype;
  int col;
  if (freqtype == 0) {
    col = 5;
  } else {
    col = 14;
  }
  lcd.setCursor(col,row);
  //sprintf(buffer,"%f",freq);
  lcd.print(freq);
}
