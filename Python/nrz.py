# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 09:14:06 2016

@author: crose
"""

def nrzs(nrz):
    NRZS = []
    state = nrz[0]
    for bit in nrz:
        state ^= bit ^ 1
        NRZS.append(state)
    return NRZS
    
        
    
NRZ = [0,0,1,0,1,1,0,1,0,0,1,0,1,1,1,0]
NRZI = NRZ[::2]
NRZQ = NRZ[1::2]

NRZS = nrzs(NRZ)
NRZSI = nrzs(NRZI)
NRZSQ = nrzs(NRZQ)

print NRZ
print NRZI
print NRZQ

print NRZS
print NRZSI
print NRZSQ
