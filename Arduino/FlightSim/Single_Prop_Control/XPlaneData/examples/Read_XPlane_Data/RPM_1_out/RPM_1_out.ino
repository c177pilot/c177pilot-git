/*
I/O interface for X-Plane Cockpit Simulator
using XPlaneData library

Receive DATA from X-Plane sample code

( to receive all data check Data Groups #37 
in "DATA Input & Output" menu of X-Plane) 

Vlad Sychev, 2014
http://svglobe.com
 */

#include <SPI.h>         
#include <Ethernet.h>
#include <XPlaneData.h>         // -- include XPlaneData library
//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8, 0};                     // X-Plane IP address ( ! set your !)
//IPAddress ip;                                   // Arduino IP address (Auto)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)
//--------------------------------------------------

float RPM1;      // RPM #1

//---------------- setup ------
void setup()  { 

   XP.begin();      //==! init XPlaneData 
 
 XP.cmd_in("none/none");   // --  NULL command to see our Arduino IP in X-Plane "Net connections" screen
                                  
 Serial.begin(9600); 
   } 
           
//------------------------ start --------------------
void loop() 
{               
 
  
//----------- Receive data from X-Plane -----------------

if (XP.udpin())   // - must be
    {              

       // - define group number (37) and position in this group (1) to get RPM parameter

       RPM1 = XP.Dget(37,1);  Serial.println(RPM1);   //Print RPM #1 
       
       
       // use the XPData object  with "Dget" function to get any other Data, with two arguments
       //  - the first number is group number, second is position of the parameter in this group

    }


 } 
 
//======== svglobe.com ================
