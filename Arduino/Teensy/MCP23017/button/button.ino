#include <Wire.h>
#include "Adafruit_MCP23017.h"
#define MCP_INPUT_PIN 0
#define MCP_ADDRESS 0

// Basic pin reading and pullup test for the MCP23017 I/O expander
// public domain!

// Connect pin #12 of the expander to Analog 5 (i2c clock)
// Connect pin #13 of the expander to Analog 4 (i2c data)
// Connect pins #15, 16 and 17 of the expander to ground (address selection)
// Connect pin #9 of the expander to 5V (power)
// Connect pin #10 of the expander to ground (common ground)
// Connect pin #18 through a ~10kohm resistor to 5V (reset pin, active low)

// Input #0 is on pin 21 so connect a button or switch from there to ground

Adafruit_MCP23017 mcp;
  
void setup() {  


  for (int k=0;k<10; k++) {
    pinMode(k,INPUT_PULLUP);
  }
  
  mcp.begin(MCP_ADDRESS);      // use default address 0
  for (int k=0;k<16;k++) {
    mcp.pinMode(k, INPUT);
    delay(10);
    mcp.pullUp(k,HIGH);
  }

  pinMode(13, OUTPUT);  // use the p13 LED as debugging
  Serial.begin(9600);
  Serial.println("I'm Alive");
}



void loop() {
  // The LED will 'echo' the button
  for (int k=0; k<16; k++) {
    Serial.print(mcp.digitalRead(k));
  }
  Serial.println();

  for (int k=0;k<10;k++) {
    Serial.print(digitalRead(k));
  }
  //Serial.println();
  //digitalWrite(13, mcp.digitalRead(MCP_INPUT_PIN));
  
  delay(500);
}
