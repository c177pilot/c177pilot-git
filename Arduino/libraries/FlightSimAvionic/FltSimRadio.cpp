#include "FltSimRadioInterface.h"
#include <Encoder.h>
#include <Bounce.h>

fltSimRadio:fltSimRadio() {
}

fltSimRadio:run() {
  FlightSim.update(); //push and pull all referenced X-plane variables

  l_ComFreq_stdby_khz[RadioTuneSelect] = *ComFreq_khz[RadioTuneSelect];  
  l_ComFreq_stdby_Mhz[RadioTuneSelect] = *ComFreq_Mhz[RadioTuneSelect];    
  l_NavFreq_stdby_khz[RadioTuneSelect] = *NavFreq_khz[RadioTuneSelect];
  l_NavFreq_stdby_Mhz[RadioTuneSelect] = *NavFreq_Mhz[RadioTuneSelect];

  // read the rotary encoders
  com_enc_fine[RadioTuneSelect] = EncComFine.read() / 2;
  com_enc_coarse[RadioTuneSelect] = EncComCoarse.read() / 2;
  nav_enc_fine[RadioTuneSelect] = EncNavFine.read() / 2;
  nav_enc_coarse[RadioTuneSelect] = EncNavCoarse.read() / 2;
  
  l_ComFreq_stdby_Mhz[RadioTuneSelect] += (com_enc_coarse[RadioTuneSelect] - com_enc_coarse_prev[RadioTuneSelect]);
  l_ComFreq_stdby_khz[RadioTuneSelect] += (com_enc_fine[RadioTuneSelect] - com_enc_fine_prev[RadioTuneSelect])*25;
  l_NavFreq_stdby_Mhz[RadioTuneSelect] += (nav_enc_coarse[RadioTuneSelect] - nav_enc_coarse_prev[RadioTuneSelect]);
  l_NavFreq_stdby_khz[RadioTuneSelect] += (nav_enc_fine[RadioTuneSelect] - nav_enc_fine_prev[RadioTuneSelect])*5;
  
  com_enc_coarse_prev[RadioTuneSelect] = com_enc_coarse[RadioTuneSelect];
  com_enc_fine_prev[RadioTuneSelect] = com_enc_fine[RadioTuneSelect];
  nav_enc_coarse_prev[RadioTuneSelect] = nav_enc_coarse[RadioTuneSelect];
  nav_enc_fine_prev[RadioTuneSelect] = nav_enc_fine[RadioTuneSelect];

  //Handle rollover of the coarse tuning
  if (l_NavFreq_stdby_Mhz[RadioTuneSelect] > 118) {
    l_NavFreq_stdby_Mhz[RadioTuneSelect] = 108;
  }  
  if (l_NavFreq_stdby_Mhz[RadioTuneSelect] < 108) {
    l_NavFreq_stdby_Mhz[RadioTuneSelect] = 118;
  }
  if (l_ComFreq_stdby_Mhz[RadioTuneSelect] < 118) {
    l_ComFreq_stdby_Mhz[RadioTuneSelect] = 136;
  }
  if (l_ComFreq_stdby_Mhz[RadioTuneSelect] > 136) {
    l_ComFreq_stdby_Mhz[RadioTuneSelect] = 118;
  }
  
  //Handle rollover of the fine tuning
  if (l_NavFreq_stdby_khz[RadioTuneSelect] > 95) {
    l_NavFreq_stdby_khz[RadioTuneSelect] = 0;
  }
  if (l_NavFreq_stdby_khz[RadioTuneSelect] < 0) {
    l_NavFreq_stdby_khz[RadioTuneSelect] = 95;
  }
  if (l_ComFreq_stdby_khz[RadioTuneSelect] > 975) {
    l_ComFreq_stdby_khz[RadioTuneSelect] = 0;
  }
  if (l_ComFreq_stdby_khz[RadioTuneSelect] < 0) {
    l_ComFreq_stdby_khz[RadioTuneSelect] = 975;
  }

  //Update X-plane dataref's
  if (l_ComFreq_stdby_Mhz[RadioTuneSelect] != l_ComFreq_stdby_Mhz_prev[RadioTuneSelect]) {
    *ComFreq_Mhz[RadioTuneSelect] = l_ComFreq_stdby_Mhz[RadioTuneSelect];
    l_ComFreq_stdby_Mhz_prev[RadioTuneSelect] = l_ComFreq_stdby_Mhz[RadioTuneSelect];
  }
  if (l_ComFreq_stdby_khz[RadioTuneSelect] != l_ComFreq_stdby_khz_prev[RadioTuneSelect]) {
    *ComFreq_khz[RadioTuneSelect] = l_ComFreq_stdby_khz[RadioTuneSelect];
    l_ComFreq_stdby_khz_prev[RadioTuneSelect] = l_ComFreq_stdby_khz[RadioTuneSelect];
  }
  if (l_NavFreq_stdby_Mhz[RadioTuneSelect] != l_NavFreq_stdby_Mhz_prev[RadioTuneSelect]) {
    *NavFreq_Mhz[RadioTuneSelect] = l_NavFreq_stdby_Mhz[RadioTuneSelect];
    l_NavFreq_stdby_Mhz_prev[RadioTuneSelect] = l_NavFreq_stdby_Mhz[RadioTuneSelect];
  }
  if (l_NavFreq_stdby_khz[RadioTuneSelect] != l_NavFreq_stdby_khz_prev[RadioTuneSelect]) {
    *NavFreq_khz[RadioTuneSelect] = l_NavFreq_stdby_khz[RadioTuneSelect];
    l_NavFreq_stdby_khz_prev[RadioTuneSelect] = l_NavFreq_stdby_khz[RadioTuneSelect];
  }

  }