#ifndef FLTSIMAUDIOPANELINTERFACE_H
#define FLTSIMAUDIOPANELINTERFACE_H
#include "FLTSIMAUDIOPANELINTERFACE.h"

/*
  FltSimAudioPanelInterface
  created 29-January-2016 V 0.1
  By Chris Rose
*/

class fltSimAudioPanelInterface
{
	private:
	Bounce ComMonSwitch[2] = {Bounce(COM1MONITORSWITCHPIN,DEBOUNCETIME), Bounce(COM2MONITORSWITCHPIN,DEBOUNCETIME)};
	Bounce NavMonSwitch[2] = {Bounce(NAV1MONITORSWITCHPIN,DEBOUNCETIME), Bounce(NAV2MONITORSWITCHPIN,DEBOUNCETIME)};
	Bounce DmeMonSwitch = Bounce(DMEMONITORSWITCHPIN,DEBOUNCETIME);
	Bounce MkrMonSwitch = Bounce(MKRMONITORSWITCHPIN,DEBOUNCETIME);
	
	public:
	fltSimAudioPanel(int comNum,void* ComEnc[2],void* NavEnc[2], void Stdby, boolean flipflop)
};
#endif