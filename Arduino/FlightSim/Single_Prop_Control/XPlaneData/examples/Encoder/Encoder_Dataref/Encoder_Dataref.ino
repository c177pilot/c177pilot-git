/*
sample code for one encoder
using XPlaneData Library
for X-Plane

Vlad
http://arduino.svglobe.com
 */

//------------------------------------
#include <SPI.h>         
#include <Ethernet.h>
#include <Wire.h>
#include <XPlaneData.h>         // -- include XPlaneData library
//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8, 0};                     // X-Plane IP address ( ! set your !)
//IPAddress ip;                                   // Arduino IP address (Auto)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)
//------------------------------------


float D1;  // - variable for dataref, that encoder controls
//------------------------------------


void setup() {
  
  XP.Pins_In (0, 12);       // Define pins for inputs (first, last including)
     
   XP.begin();      //==! init XPlaneData 
 
 
//-- initiate data when Arduino starts (if needed):

 D1 = XP.Out_ini(98,1);  // Get data for encoder, (Nav1 OBS, HSI) write to  D1 variable -
                          // first - check box # 98 in X-Plane! 
 
   }
     
//==============
void loop() 
{

       //================== Encoder on the pins  #2-3 , change dataref (D1 variable) ==================


D1 = XP.Encoder_Dref(2, "cockpit2/radios/actuators/hsi_obs_deg_mag_pilot[0]", D1, 1, 0);

// ------------ encoder for for dataref arguments:
                     // 1 -first pin number 
                     // 2 Dataref, 
                     // 3 Increment/decrement Value, 
                     // 4  encoder type (0-3)
                     
                     
       //================== Encoder with Button on the pins  #6-7 , with button on pin #10 ==================

D1 = XP.Encoder_2xDref(6, 10, "cockpit2/radios/actuators/hsi_obs_deg_mag_pilot[0]", D1, 1, 10, 0);

// ------------ encoder for for dataref arguments:
                     // 1 - first pin number 
                     // 2 - Dataref, 
                     // 3 - Increment/decrement Value when button not pressed 
                     // 4 - Increment/decrement Value when button is pressed
                     // 5 - encoder type (0-3)                     
                     
 
}   
