#include <adcSwitch.h>

#define ENCODER_DO_NOT_USE_INTERRUPTS //necessary if the dedicated interrupts are not being used.
#define DEBOUNCETIME 50
#define LONGPRESS 2000
#define FLAP_POSITIONS 4 // Cessna type: 0,10,20,30. This must match the physical number of positions for the flaps switch
#define STARTER_POSITIONS 5 //0=off, 1=left mag, 2=right mag, 3=both, 4=start
#define RADIO_TUNE_SELECT_POSITIONS 4 //NAVCOM1 = 0, NAVCOM2 = 1, XPONDER = 2, GPS = 3

#include <Encoder.h>
#include <Bounce.h>
#include <ADC.h>
#include "adcSwitch.h"

//IntervalTimer wdTimer;
IntervalTimer TenHzLoopTimer;

enum RADIOTUNESELECT_t {NAVCOM1 = 0, NAVCOM2 = 1, XPONDER = 2, TBD=3};

/*Change Log
 * C Rose.
 * 30-Jan-2016 All features are working
*/

//Only compatible with Teensy 3.1 MCU.

//define switch pins
const int RADIOTXSWITCHPIN = 0;
const int COM1MONITORSWITCHPIN=1;
const int COM2MONITORSWITCHPIN=2;
const int NAV1MONITORSWITCHPIN=3;
const int NAV2MONITORSWITCHPIN=4;
const int DMEMONITORSWITCHPIN=5;
const int MKRMONITORSWITCHPIN=6;
const int CARBHEATSWITCHPIN = 7;
const int NAVFLIPFLOPSWITCHPIN=8;
const int COMFLIPFLOPSWITCHPIN=9;

//define the NAV encoder pins
const int ENCNAVCOARSEPIN_A=10;
const int ENCNAVCOARSEPIN_B=11;
const int ENCNAVFINEPIN_A=12;
const int ENCNAVFINEPIN_B=13;

//define the COM encoder pins
const int ENCCOMCOARSEPIN_A=14;
const int ENCCOMCOARSEPIN_B=15;
const int ENCCOMFINEPIN_A=16;
const int ENCCOMFINEPIN_B=17;

//define pins used for analog inputs
const int RADIOTUNEPOTPIN = A7;
const int FLAPRQSTPOTPIN = A8;
const int ELEVTRIMPOTPIN = A6;
const int STARTERPOTPIN = A9;

//define Bounce objects for momentary contact switches
Bounce ComFlipFlopSwitch = Bounce(COMFLIPFLOPSWITCHPIN, DEBOUNCETIME);
Bounce NavFlipFlopSwitch = Bounce(NAVFLIPFLOPSWITCHPIN, DEBOUNCETIME);
Bounce ComMonSwitch[2] = {Bounce(COM1MONITORSWITCHPIN,DEBOUNCETIME), Bounce(COM2MONITORSWITCHPIN,DEBOUNCETIME)};
Bounce NavMonSwitch[2] = {Bounce(NAV1MONITORSWITCHPIN,DEBOUNCETIME), Bounce(NAV2MONITORSWITCHPIN,DEBOUNCETIME)};
Bounce DmeMonSwitch = Bounce(DMEMONITORSWITCHPIN,DEBOUNCETIME);
Bounce MkrMonSwitch = Bounce(MKRMONITORSWITCHPIN,DEBOUNCETIME);
Bounce RadioTXSwitch = Bounce(RADIOTXSWITCHPIN,DEBOUNCETIME);              //Determine which radio is active for TX
Bounce CarbHeatSwitch = Bounce(CARBHEATSWITCHPIN,DEBOUNCETIME);

Encoder EncNavCoarse = Encoder(ENCNAVCOARSEPIN_A,ENCNAVCOARSEPIN_B);// Rotary Encoder A channel pin and B channel pin
Encoder EncNavFine = Encoder(ENCNAVFINEPIN_A,ENCNAVFINEPIN_B);

Encoder EncComCoarse = Encoder(ENCCOMCOARSEPIN_A,ENCCOMCOARSEPIN_B);
Encoder EncComFine = Encoder(ENCCOMFINEPIN_A,ENCCOMFINEPIN_B);

//create the Teensy ADC object that will be used by the analog switch axes
ADC *adc = new ADC();

long l_NavFreq_stdby_khz[2], l_NavFreq_stdby_Mhz[2], l_NavFreq_stdby_khz_prev[2], l_NavFreq_stdby_Mhz_prev[2];
long l_ComFreq_stdby_khz[2], l_ComFreq_stdby_Mhz[2], l_ComFreq_stdby_khz_prev[2], l_ComFreq_stdby_Mhz_prev[2];
long nav_enc_coarse[2]={0,0};
long nav_enc_fine[2]={0,0};
long com_enc_coarse[2]={0,0};
long com_enc_fine[2]={0,0};

long nav_enc_coarse_prev[2]={0,0};
long nav_enc_fine_prev[2]={0,0};
long com_enc_coarse_prev[2]={0,0};
long com_enc_fine_prev[2]={0,0};

RADIOTUNESELECT_t RadioTuneSelect = NAVCOM1 , RadioTuneSelect_prev = NAVCOM1;
int TuneStdby[2]={1,1};  //True, tunes the standby side of the radio, false tunes primary side. A long hold of COM1 monitor will toggle this mode for COM1, likewise for COM2
int TuneStdbyGuard[2] = {false, false};
boolean StarterEngaged=false;

// Create X-Plane objects
FlightSimCommand ComFlipFlop[2], NavFlipFlop[2], engineStart1;;
FlightSimInteger NavFreq_stdby_Mhz[2], NavFreq_stdby_khz[2];
FlightSimInteger ComFreq_stdby_Mhz[2], ComFreq_stdby_khz[2];
FlightSimInteger NavFreq_primary_Mhz[2], NavFreq_primary_khz[2];
FlightSimInteger ComFreq_primary_Mhz[2], ComFreq_primary_khz[2];
FlightSimInteger *ComFreq_Mhz[2], *ComFreq_khz[2], *NavFreq_Mhz[2], *NavFreq_khz[2];
FlightSimInteger FlapDetents;
FlightSimInteger ComMonSelect[2], NavMonSelect[2], DmeMonSelect, MkrMonSelect, RadioTXSelect, IgnitionKeyPosition;
FlightSimFloat ElevTrim, FlapRqst, CarbHeatRatio;

//define switches that are tied to analog axes
adcSwitch radioTuneADCSwitch = adcSwitch(adc, RADIOTUNEPOTPIN, RADIO_TUNE_SELECT_POSITIONS);
adcSwitch flapRqstADCSwitch = adcSwitch(adc, FLAPRQSTPOTPIN, FLAP_POSITIONS);
adcSwitch starterADCSwitch = adcSwitch(adc, STARTERPOTPIN, STARTER_POSITIONS);

// setup runs once, when Teensy boots.
void setup() {
  Serial.begin(9600);
  Serial.println("Inside Setup()");
  
  //setup the Teensy ADC
  adc->setReference(ADC_REF_3V3, ADC_0);
  adc->setResolution(16,ADC_0);
  adc->setSamplingSpeed(ADC_VERY_LOW_SPEED,ADC_0);
  adc->setAveraging(8,ADC_0);

  // bind variables to the X-Plane datarefs
  //The opaque data references can be found here: ./<X-plane>/Resources/plugins/Commands.txt
  NavFreq_stdby_Mhz[0] = XPlaneRef("sim/cockpit2/radios/actuators/nav1_standby_frequency_Mhz");
  NavFreq_stdby_Mhz[1] = XPlaneRef("sim/cockpit2/radios/actuators/nav2_standby_frequency_Mhz");
  NavFreq_stdby_khz[0] = XPlaneRef("sim/cockpit2/radios/actuators/nav1_standby_frequency_khz");
  NavFreq_stdby_khz[1] = XPlaneRef("sim/cockpit2/radios/actuators/nav2_standby_frequency_khz");

  ComFreq_stdby_Mhz[0] = XPlaneRef("sim/cockpit2/radios/actuators/com1_standby_frequency_Mhz");
  ComFreq_stdby_Mhz[1] = XPlaneRef("sim/cockpit2/radios/actuators/com2_standby_frequency_Mhz");
  ComFreq_stdby_khz[0] = XPlaneRef("sim/cockpit2/radios/actuators/com1_standby_frequency_khz");
  ComFreq_stdby_khz[1] = XPlaneRef("sim/cockpit2/radios/actuators/com2_standby_frequency_khz");
  
  NavFreq_primary_Mhz[0] = XPlaneRef("sim/cockpit2/radios/actuators/nav1_frequency_Mhz");
  NavFreq_primary_Mhz[1] = XPlaneRef("sim/cockpit2/radios/actuators/nav2_frequency_Mhz");
  NavFreq_primary_khz[0] = XPlaneRef("sim/cockpit2/radios/actuators/nav1_frequency_khz");
  NavFreq_primary_khz[1] = XPlaneRef("sim/cockpit2/radios/actuators/nav2_frequency_khz");

  ComFreq_primary_Mhz[0] = XPlaneRef("sim/cockpit2/radios/actuators/com1_frequency_Mhz");
  ComFreq_primary_Mhz[1] = XPlaneRef("sim/cockpit2/radios/actuators/com2_frequency_Mhz");
  ComFreq_primary_khz[0] = XPlaneRef("sim/cockpit2/radios/actuators/com1_frequency_khz");
  ComFreq_primary_khz[1] = XPlaneRef("sim/cockpit2/radios/actuators/com2_frequency_khz");
 
  //Set the Com and Nav tuning to standby initially
  for (int k=0;k<2;k++) {
    ComFreq_Mhz[k] = &ComFreq_stdby_Mhz[k];
    ComFreq_khz[k] = &ComFreq_stdby_khz[k];
    NavFreq_Mhz[k] = &NavFreq_stdby_Mhz[k];
    NavFreq_khz[k] = &NavFreq_stdby_khz[k];
  }
  
  ComFlipFlop[0] = XPlaneRef("sim/radios/com1_standy_flip"); //these datarefs are not published in the online X-plane datarefs
  ComFlipFlop[1] = XPlaneRef("sim/radios/com2_standy_flip");  
  NavFlipFlop[0] = XPlaneRef("sim/radios/nav1_standy_flip");
  NavFlipFlop[1] = XPlaneRef("sim/radios/nav2_standy_flip");  

  RadioTXSelect = XPlaneRef("sim/cockpit2/radios/actuators/audio_com_selection"); //6=com1, 7=com2
  //could have also used: 'sim/audio_panel/transmit_audio_com1' and 'sim/audio_panel/transmit_audio_com2'
  ComMonSelect[0] = XPlaneRef("sim/cockpit2/radios/actuators/audio_selection_com1");
  ComMonSelect[1] = XPlaneRef("sim/cockpit2/radios/actuators/audio_selection_com2");
  NavMonSelect[0] = XPlaneRef("sim/cockpit2/radios/actuators/audio_selection_nav1");
  NavMonSelect[1] = XPlaneRef("sim/cockpit2/radios/actuators/audio_selection_nav2");
  DmeMonSelect = XPlaneRef("sim/cockpit2/radios/actuators/audio_dme_enabled");
  MkrMonSelect = XPlaneRef("sim/cockpit2/radios/actuators/audio_marker_enabled");

// for engine starter example: https://simelectronics.wordpress.com/2013/07/28/ignition-key-example/
  engineStart1 = XPlaneRef("sim/starters/engage_starter_1");
  IgnitionKeyPosition = XPlaneRef("sim/cockpit2/engine/actuators/ignition_on[0]"); //may need "sim/starters/engage_starter_1" to engage starter

  FlapDetents = XPlaneRef("sim/aircraft/controls/acf_flap_detents"); //to determine number of flap positions for loaded aircraft
  ElevTrim = XPlaneRef("sim/flightmodel/controls/elv_trim");
  FlapRqst = XPlaneRef("sim/flightmodel/controls/flaprqst"); //might be sim/cockpit2/controls/flap_ratio
  CarbHeatRatio = XPlaneRef("sim/cockpit2/engine/actuators/carb_heat_ratio");

  //configure digital input pins for PULLUP
  pinMode(COMFLIPFLOPSWITCHPIN, INPUT_PULLUP);  // input pullup mode allows connecting
  pinMode(NAVFLIPFLOPSWITCHPIN, INPUT_PULLUP);  // buttons and switches from the pins
  pinMode(COM1MONITORSWITCHPIN, INPUT_PULLUP);
  pinMode(COM2MONITORSWITCHPIN, INPUT_PULLUP);
  pinMode(NAV1MONITORSWITCHPIN, INPUT_PULLUP);
  pinMode(NAV2MONITORSWITCHPIN, INPUT_PULLUP);
  pinMode(DMEMONITORSWITCHPIN, INPUT_PULLUP);
  pinMode(MKRMONITORSWITCHPIN, INPUT_PULLUP);
  pinMode(RADIOTXSWITCHPIN, INPUT_PULLUP);  
  pinMode(CARBHEATSWITCHPIN, INPUT_PULLUP);
  pinMode(ENCCOMCOARSEPIN_A, INPUT_PULLUP);
  pinMode(ENCCOMCOARSEPIN_B, INPUT_PULLUP);
  pinMode(ENCCOMFINEPIN_A, INPUT_PULLUP);
  pinMode(ENCCOMFINEPIN_B, INPUT_PULLUP); 
  pinMode(ENCNAVCOARSEPIN_A, INPUT_PULLUP);
  pinMode(ENCNAVCOARSEPIN_B, INPUT_PULLUP);
  pinMode(ENCNAVFINEPIN_A, INPUT_PULLUP);
  pinMode(ENCNAVFINEPIN_B, INPUT_PULLUP);
  
  Serial.begin(9600);

  TenHzLoopTimer.begin(TenHzLoop, 100000); //service the flaps and trim every 1/10 sec
} //end of setup

void TenHzLoop() {
  
  ServiceAudioPanel();

  
  Serial.println(flapRqstADCSwitch.value());
  FlapRqst = flapsValueMap(flapRqstADCSwitch.value(), FlapDetents);
  ElevTrim = (float)2*adc->analogRead(ELEVTRIMPOTPIN)/65536 - 1;

  //handle the starter key switch
  IgnitionKeyPosition = starterADCSwitch.value();
  if (IgnitionKeyPosition == 4) {
    engineStart1.begin();
    StarterEngaged = true;
  }

  if ((IgnitionKeyPosition < 4) && (StarterEngaged == true)) {
    engineStart1.end();
    StarterEngaged = false;
  }
  //Serial.println(IgnitionKeyPosition);
  //end starter key switch

  if (CarbHeatSwitch.update()) {
    if (CarbHeatSwitch.fallingEdge()) {
      CarbHeatRatio = 1;
    }
    
    if (CarbHeatSwitch.risingEdge()) {
      CarbHeatRatio = 0;
    }
  }

  if (radioTuneADCSwitch.update()) {
      RadioTuneSelect = (RADIOTUNESELECT_t)radioTuneADCSwitch.value();
      if (RadioTuneSelect>NAVCOM2) RadioTuneSelect=NAVCOM2; //no other radio's being handled right now

      com_enc_coarse_prev[RadioTuneSelect_prev] = 0;
      com_enc_fine_prev[RadioTuneSelect_prev] = 0;
      nav_enc_coarse_prev[RadioTuneSelect_prev] = 0;
      nav_enc_fine_prev[RadioTuneSelect_prev] = 0;
      
      EncComFine.write(0);
      EncComCoarse.write(0);
      EncNavFine.write(0);
      EncNavCoarse.write(0);

      RadioTuneSelect_prev = RadioTuneSelect;
  }

  // read the switch for COM and NAV flip-flop switches. Only the selected radio should be switched.
  if (NavFlipFlopSwitch.update()) {
    if (NavFlipFlopSwitch.fallingEdge()) {
      NavFlipFlop[RadioTuneSelect] = 1;
    } 
  }
  
  if (ComFlipFlopSwitch.update()) { //read the physical switch
    if (ComFlipFlopSwitch.fallingEdge()) {
      ComFlipFlop[RadioTuneSelect] = 1;
    } 
  }
} //end the 10Hz Loop

// loop runs repetitively, as long as Teensy is powered up
void loop() {

    // normally the first step in loop() should update from X-Plane  
  FlightSim.update(); //push and pull all referenced X-plane variables
  //Serial.println("Update done");

  l_ComFreq_stdby_khz[RadioTuneSelect] = *ComFreq_khz[RadioTuneSelect];  
  l_ComFreq_stdby_Mhz[RadioTuneSelect] = *ComFreq_Mhz[RadioTuneSelect];    
  l_NavFreq_stdby_khz[RadioTuneSelect] = *NavFreq_khz[RadioTuneSelect];
  l_NavFreq_stdby_Mhz[RadioTuneSelect] = *NavFreq_Mhz[RadioTuneSelect];

  // read the rotary encoders
  com_enc_fine[RadioTuneSelect] = EncComFine.read() / 2;
  com_enc_coarse[RadioTuneSelect] = EncComCoarse.read() / 2;
  nav_enc_fine[RadioTuneSelect] = EncNavFine.read() / 2;
  nav_enc_coarse[RadioTuneSelect] = EncNavCoarse.read() / 2;
  
  l_ComFreq_stdby_Mhz[RadioTuneSelect] += (com_enc_coarse[RadioTuneSelect] - com_enc_coarse_prev[RadioTuneSelect]);
  l_ComFreq_stdby_khz[RadioTuneSelect] += (com_enc_fine[RadioTuneSelect] - com_enc_fine_prev[RadioTuneSelect])*25;
  l_NavFreq_stdby_Mhz[RadioTuneSelect] += (nav_enc_coarse[RadioTuneSelect] - nav_enc_coarse_prev[RadioTuneSelect]);
  l_NavFreq_stdby_khz[RadioTuneSelect] += (nav_enc_fine[RadioTuneSelect] - nav_enc_fine_prev[RadioTuneSelect])*5;
  
  com_enc_coarse_prev[RadioTuneSelect] = com_enc_coarse[RadioTuneSelect];
  com_enc_fine_prev[RadioTuneSelect] = com_enc_fine[RadioTuneSelect];
  nav_enc_coarse_prev[RadioTuneSelect] = nav_enc_coarse[RadioTuneSelect];
  nav_enc_fine_prev[RadioTuneSelect] = nav_enc_fine[RadioTuneSelect];

  //Handle rollover of the coarse tuning
  if (l_NavFreq_stdby_Mhz[RadioTuneSelect] > 118) {
    l_NavFreq_stdby_Mhz[RadioTuneSelect] = 108;
  }  
  if (l_NavFreq_stdby_Mhz[RadioTuneSelect] < 108) {
    l_NavFreq_stdby_Mhz[RadioTuneSelect] = 118;
  }
  if (l_ComFreq_stdby_Mhz[RadioTuneSelect] < 118) {
    l_ComFreq_stdby_Mhz[RadioTuneSelect] = 136;
  }
  if (l_ComFreq_stdby_Mhz[RadioTuneSelect] > 136) {
    l_ComFreq_stdby_Mhz[RadioTuneSelect] = 118;
  }
  
  //Handle rollover of the fine tuning
  if (l_NavFreq_stdby_khz[RadioTuneSelect] > 95) {
    l_NavFreq_stdby_khz[RadioTuneSelect] = 0;
  }
  if (l_NavFreq_stdby_khz[RadioTuneSelect] < 0) {
    l_NavFreq_stdby_khz[RadioTuneSelect] = 95;
  }
  if (l_ComFreq_stdby_khz[RadioTuneSelect] > 975) {
    l_ComFreq_stdby_khz[RadioTuneSelect] = 0;
  }
  if (l_ComFreq_stdby_khz[RadioTuneSelect] < 0) {
    l_ComFreq_stdby_khz[RadioTuneSelect] = 975;
  }

  //Update X-plane dataref's
  if (l_ComFreq_stdby_Mhz[RadioTuneSelect] != l_ComFreq_stdby_Mhz_prev[RadioTuneSelect]) {
    *ComFreq_Mhz[RadioTuneSelect] = l_ComFreq_stdby_Mhz[RadioTuneSelect];
    l_ComFreq_stdby_Mhz_prev[RadioTuneSelect] = l_ComFreq_stdby_Mhz[RadioTuneSelect];
  }
  if (l_ComFreq_stdby_khz[RadioTuneSelect] != l_ComFreq_stdby_khz_prev[RadioTuneSelect]) {
    *ComFreq_khz[RadioTuneSelect] = l_ComFreq_stdby_khz[RadioTuneSelect];
    l_ComFreq_stdby_khz_prev[RadioTuneSelect] = l_ComFreq_stdby_khz[RadioTuneSelect];
  }
  if (l_NavFreq_stdby_Mhz[RadioTuneSelect] != l_NavFreq_stdby_Mhz_prev[RadioTuneSelect]) {
    *NavFreq_Mhz[RadioTuneSelect] = l_NavFreq_stdby_Mhz[RadioTuneSelect];
    l_NavFreq_stdby_Mhz_prev[RadioTuneSelect] = l_NavFreq_stdby_Mhz[RadioTuneSelect];
  }
  if (l_NavFreq_stdby_khz[RadioTuneSelect] != l_NavFreq_stdby_khz_prev[RadioTuneSelect]) {
    *NavFreq_khz[RadioTuneSelect] = l_NavFreq_stdby_khz[RadioTuneSelect];
    l_NavFreq_stdby_khz_prev[RadioTuneSelect] = l_NavFreq_stdby_khz[RadioTuneSelect];
  }

  delay(1);
} //end the main loop()

void ServiceAudioPanel() {
    //Check COM and NAV Monitor switches. These are independent of selected radio   
  for (int k=0; k<2; k++) {
       //need to monitor for long press of ComMon switches and toggle the primary/standby tuning with long presses.
       //Serial.println(TuneStdby[1]);
       
      //switch from standby side to primary side tuning if COM[n] monitor switch is depressed for long period
    if (ComMonSwitch[k].read() == LOW && ComMonSwitch[k].duration() > LONGPRESS && TuneStdbyGuard[k]==false) {
        TuneStdbyGuard[k]=true;
         TuneStdby[k] = abs(!TuneStdby[k]);

          FlightSim.update();
         if (TuneStdby[k] == 1) {
          ComFreq_Mhz[k] = &ComFreq_stdby_Mhz[k];
          ComFreq_khz[k] = &ComFreq_stdby_khz[k];
          NavFreq_Mhz[k] = &NavFreq_stdby_Mhz[k];
          NavFreq_khz[k] = &NavFreq_stdby_khz[k];
         } else {
          ComFreq_Mhz[k] = &ComFreq_primary_Mhz[k];
          ComFreq_khz[k] = &ComFreq_primary_khz[k];
          NavFreq_Mhz[k] = &NavFreq_primary_Mhz[k];
          NavFreq_khz[k] = &NavFreq_primary_khz[k];
         }   
         
         *ComFreq_khz[k] = 0; //set the kHz of the currently selected standby/primary side to 000 to indicate the switch to the user    
    }         

    if (ComMonSwitch[k].update()) { //if the status has changed then...
     TuneStdbyGuard[k] = false;
     if (ComMonSwitch[k].fallingEdge()) {
        ComMonSelect[k] = abs(!ComMonSelect[k]); //assert the Com Monitor if falling edge change
      } 
    }
    
    if (NavMonSwitch[k].update()) {
    
      if (NavMonSwitch[k].fallingEdge()) {
        NavMonSelect[k] = abs(!NavMonSelect[k]);
      }
    }
  }

  if (RadioTXSwitch.update()) {
  
    if (RadioTXSwitch.fallingEdge()) {
      RadioTXSelect = 7; //com2 //this will turn off ComMonSelect[1]
    }
    
    if (RadioTXSwitch.risingEdge()) {
      RadioTXSelect = 6; //failsafe to com1. This will turn off ComMonSelect[0]
    }
  }

  if (MkrMonSwitch.update()) {
    if (MkrMonSwitch.fallingEdge()) {
      MkrMonSelect = abs(!MkrMonSelect);
    } 
  }

  if (DmeMonSwitch.update()) {
    if (DmeMonSwitch.fallingEdge()) {
      DmeMonSelect = abs(!DmeMonSelect);
    } 
  }
}

float flapsValueMap(int selectorPosition, int detents) {

  //This function maps the flaps selector 4-way switch (0,10,20,30 deg flaps) into the number of flap detents in the current aircraft model
  //detents is the number of non-zero flaps positions supported by the aircraft model, so a 4-position switch will support up to 3-detents.

  float flapsLUT[3][4] = {{0,1,1,1},{0, 0.5,1,1},{0, 0.33, 0.66, 1}};

  if (selectorPosition > 3) selectorPosition = 3;
  if (detents > 3) detents = 3;
  
  return flapsLUT[detents-1][selectorPosition];
}


