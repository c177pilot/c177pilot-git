/*
Using of potentiometer  as  knob for 
control rotating elements of instruments
(arrows, card for HSI, ADF, VOR)
for X-Plane

(sends data to X-Plane only opon value change, within "Dead_zone" of the sensor)

Vlad Sychev 2013
http://svglobe.com
 */

//------------------------------------
#include <SPI.h>         
#include <Ethernet.h>
#include <Wire.h>
#include <XPlaneData.h>         // -- include XPlaneData library

//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8, 0};                     // X-Plane IP address ( ! set your !)
//IPAddress ip;                                   // Arduino IP address (Auto)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)
//------------------------------------

//--- setup section ---

void setup() {

   XP.begin();      //==! init XPlaneData 
 }
     
//==========================================================================

void loop() 

{

 // ------HSI OBS course pot on the analog input #2 (min, max, precision)
 
  
XP.Analog_Input(2, "cockpit2/radios/actuators/hsi_obs_deg_mag_pilot[0]", 0, 360, 360); 
              

 }   

                
//============================================================== 

