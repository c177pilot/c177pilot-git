/*
Arduino Leonardo Joystick!

*/


JoyState_t joySt;
int pitchAxisAD = A0;    // analog input for pitch axis
int rollAxisAD = A1;     // analog input for roll axis
int pitchTrimAD = A2;
int yawTrimAD = A3;
int throttleAD = A4;
int propAD = A5;
int mixAD = A6;
int flapsAD = A7;

int cowlOpen = 8;      // select the pin for the LED
int cowlClosed=9;
int apOnOff=10;
int apHdgHold=11;
int apAltHold=12;
int apAppHold=13;
int apSelect=14;

int ledPin = 13;
int sensorValue = 0;  // variable to store the value coming from the sensor


void setup()
{
	pinMode(ledPin, OUTPUT);


	joySt.xAxis = 0;
	joySt.yAxis = 0;
	joySt.zAxis = 0;
	joySt.xRotAxis = 0;
	joySt.yRotAxis = 0;
	joySt.zRotAxis = 0;
	joySt.throttle = 0;
	joySt.rudder = 0;
	joySt.hatSw1 = 0;
	joySt.hatSw2 = 0;
	joySt.buttons = 0;

}


void loop()
{


	joySt.xAxis = (analogRead(pitchAxisAD) + analogRead(pitchAxisAD) + analogRead(pitchAxisAD) + analogRead(pitchAxisAD));
	joySt.yAxis = (analogRead(rollAxisAD) + analogRead(rollAxisAD) + analogRead(rollAxisAD) + analogRead(rollAxisAD));
	joySt.zAxis = (analogRead(pitchTrimAD) + analogRead(pitchTrimAD) + analogRead(pitchTrimAD) + analogRead(pitchTrimAD));
	joySt.xRotAxis = (analogRead(yawTrimAD) + analogRead(yawTrimAD) + analogRead(yawTrimAD) +analogRead(yawTrimAD));
	joySt.yRotAxis = (analogRead(throttleAD) + analogRead(throttleAD) +analogRead(throttleAD) +analogRead(throttleAD));
	joySt.zRotAxis = (analogRead(propAD) + analogRead(propAD) + analogRead(propAD) + analogRead(propAD));
	joySt.throttle = (analogRead(mixAD) + analogRead(mixAD) + analogRead(mixAD) + analogRead(mixAD)); 
	joySt.rudder = (analogRead(flapsAD) + analogRead(flapsAD) + analogRead(flapsAD) + analogRead(flapsAD));

//      No assigment of pins to the hardware but manipulate the data structure
//	joySt.buttons <<= 1;
//	if (joySt.buttons == 0)
//		joySt.buttons = 1;

//      No assignment of hat switch hardware
//	joySt.hatSw1++;
//	joySt.hatSw2--;
//
//	if (joySt.hatSw1 > 8)
//		joySt.hatSw1 = 0;
//	if (joySt.hatSw2 > 8)
//		joySt.hatSw2 = 8;

	delay(100);

	if (joySt.zRotAxis > 127)
		digitalWrite(ledPin, HIGH);
	else
		digitalWrite(ledPin, LOW);


	// Call Joystick.move
	Joystick.setState(&joySt);

}
