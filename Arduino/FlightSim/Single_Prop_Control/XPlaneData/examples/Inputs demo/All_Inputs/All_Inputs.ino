/*

Various input types demonstration
XPlaneData Library used

Download XPlaneData Library on the site.

  by Vlad Sychev 2014
http://arduino.svglobe.com
 */

//***** library ******************************************************************************

#include <SPI.h>         
#include <Ethernet.h>
#include <XPlaneData.h>         // -- include XPlaneData library
//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8, 0};                     // X-Plane IP address ( ! set your !)
//IPAddress ip;                                   // Arduino IP address (Auto)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)

//--------------------------------------- Analog Inputs-----------------------------


//----------------------
void setup()
      {
   XP.begin();      //==! init XPlaneData 
 
   XP.Pins_In (0, 49);   //Define pins for inputs (first, last including)   

  }
//--------------------------
void loop() 

{
  //------------Analog inputs using------------
  
 // ----Analog inputs for 3-Axis (yoke) pins A0, A1, A2 -------------

XP.Analog_Input(0, "cockpit2/controls/yoke_pitch_ratio[0]", -1, 1, 500);  // Pitch yoke, precision 500
XP.Analog_Input(1, "cockpit2/controls/yoke_roll_ratio[0]", -1, 1, 500);    // Roll  yoke, precision 500
XP.Analog_Input(2, "cockpit2/controls/yoke_heading_ratio[0]", -1, 1, 500);  // Rudder, precision 1000
 
//----Analog input for Flaps control lever pin# A3 (5 position precision, you can choose other precision):
 
XP.Analog_Input(3, "cockpit2/controls/flap_ratio[0]", 0, 1, 5);     // Flaps, 5 positions precision


//=========  Analog inputs using as Rotary swith ( function "RotarySwitch" used) ==========

   //---Rotary switch for magneto/starter

 if (XP.RotarySwitch(7, 5)) 
     {
      XP.Button_Cmd(1, "magnetos/magnetos_off_1");              // OFF  
      XP.Button_Cmd(2, "magnetos/magnetos_right_1");            // RIGHT
      XP.Button_Cmd(3, "magnetos/magnetos_left_1");             // LEFT
      XP.Button_Cmd(4, "magnetos/magnetos_both_1");             // BOTH
      XP.Button_Rep(5, "starters/engage_starter_1");            // START ( continuously )
     }

 //=============================================== Digital inputs section ===================================================


  // Pin## 4, 10, 13 not used
                                                       
// Button - momentary action (toggle commands)

XP.Button_Cmd(0, "radios/com1_standy_flip");                  //--  Com1/Com1-stby flip
 
 // Buttons Increment/decrement (with repeating after 0.8 sec of holding) 

XP.Button_Inc(1, "autopilot/heading_up");   //  +    
XP.Button_Inc(2, "autopilot/heading_down");   // -

 // ---- Toggle switches                 

XP.Switch_Com (3, "systems/avionics_off", "systems/avionics_on");                      // Master Avionics switch    
XP.Switch_Com (6, "lights/landing_lights_on", "lights/landing_lights_off");   // Landing light  switch  
 

// Buttons (as )
// Gear lever buttons:

XP.Button_Cmd(8, "flight_controls/landing_gear_down");                    //---- GEAR DN    
XP.Button_Cmd(9, "flight_controls/landing_gear_up");                      //---- GEAR UP   



  // -- continuously repeating COMMAND button (send command continuously while pressed):

XP.Button_Rep(24, "annunciator/test_all_annunciators");        //--- Ann Test Button  

//----- continuously repeating DATAREF button (send dataref value continuously while pressed):

XP.Button_RDf(8, "cockpit2/engine/actuators/ignition_key[0]", 4);  // 


// ---- Button - Set specific value to dataref :

XP.Set_Value(7, "cockpit2/engine/actuators/ignition_key[0]", 2); 


//------  Key inputs

XP.Button_Key (31, "q");  // 
XP.Button_Chr (34, 27);   // 

//=================================== Encoders Section ==================

// -----! Pin #39  - Encoders internal  Push Button for Com/Nav (If Pressed - fine up/dn)
 
XP.Encoder_2xCom(11, 12, "radios/stby_com2_coarse_down", "radios/stby_com2_coarse_up", 
"radios/stby_com2_fine_down","radios/stby_com2_fine_up", 0);        
            


 }  


