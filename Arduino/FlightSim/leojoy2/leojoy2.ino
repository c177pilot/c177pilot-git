/*
Arduino Leonardo Joystick!

15-Dec-2014 - Added a dither to the polling loop to force X-plane to see the changes on analog inputs
16-Dec-2014 - Spread out the multi sampling of the AD's 
16-Dec-2014 - Added 64x sampling to provide 16-bit ADC samples to the USB HID. The Leonardo provides 10-bit ADC's so each can be oversamples by 64x to gain a full 16-bit word. 
*/

#define JOYHID_ENABLED 1
JoyState_t joySt;

int ledPin = 13;
int dither = 0;

void setup()
{
	pinMode(ledPin, OUTPUT);
  //Serial.begin(9600); //may interfere with USB messages
}


void loop()
{
	for (uint8_t ind=0; ind<8; ind++) {
	    joySt.axis[ind] = 0;
	}

  
  //Leonardo provides 10-bit input. USB HID spec allows 16-bit inputs (a gain of 6-bits or 64x)
  //However, the HID descriptor is only for 8-bits. TODO - change descriptor to 16-bit analog values
	/* Move all of the axes */
    for (int i=0;i<64;i++) {
	for (uint8_t ind=0; ind<8; ind++) {
	    joySt.axis[ind] += (int16_t)analogRead(ind)-512;
	}
    }
    
    //add +/- one ADC count each time through the loop. This should be beyond the bandwidth of the X-plane model to respond
//    dither++;
//    if (dither > 1) dither=0;
//    
//    joySt.xAxis += dither;
//    joySt.yAxis += dither;
//    joySt.zAxis += dither;
//    joySt.xRotAxis += dither;
//    joySt.yRotAxis += dither;
//    joySt.zRotAxis += dither;
//    joySt.throttle += dither;
//    joySt.rudder += dither;



//      No assigment of pins to the hardware but manipulate the data structure
	joySt.buttons[0] <<= 1;
	if (joySt.buttons[0] == 0)
		joySt.buttons[0] = 1;

	joySt.buttons[1] <<= 1;
	if (joySt.buttons[1] == 0)
		joySt.buttons[1] = 1;

	joySt.buttons[2] <<= 1;
	if (joySt.buttons[2] == 0)
		joySt.buttons[2] = 1;

	joySt.buttons[3] <<= 1;
	if (joySt.buttons[3] == 0)
		joySt.buttons[3] = 1;

        //Serial.println(analogRead(0));
        //Serial.println(joySt.axis[0]);
        
	if (joySt.axis[0] > 0)
		digitalWrite(ledPin, HIGH);
	else
		digitalWrite(ledPin, LOW);


	// Call Joystick.move
	Joystick.setState(&joySt);
	delay(10);  

}
