/*
  XPlaneData
  Released into the public domain.
	Vlad Sychev 2014
*/
#ifndef XPlaneData_h
#define XPlaneData_h
#include "Arduino.h"
#include <Wire.h>
//#include <IPAddress.h>
#include <Ethernet.h>
//#include <ArdExt.h>
//#include "EthernetUdp.h"

#define COM "CMND0sim/"
#define DREF "DREF0"
#define KEY "CHAR0"
#define MENU "MENU0"

class XPlaneData
{
  public:

    XPlaneData( int XPR, int XPW, int IPx[5], byte mc[6]); 
    void begin ();      				 // init
    boolean udpin(); 							// read and check UDP packets
	void Pins_In(int first, int last);
	void Pins_Out(int first, int last);
	float Out_ini(int grp, int idx); 
//	void SetData_Out(int D_out); 
// analog input
    void Analog_Input(int in, char dref[], long v1, long v2, float pre); 
	boolean A_Read(int in, float numb); 	// multibutton on analog input
	boolean ButtonArray(int in, int numb); 
	boolean RotarySwitch(int in, int numb);             // Rotary switch on analog input
// output
    float Dget(int grp, int par);					// get parameter from X-Plane (#group, # param)
// digital input
	int D_read(int in);					// read one digital in
	void Button_Cmd(int in, char* D_com);						// Command button for commans
	void Button_Inc(int in, char* D_com);						// Increment/Decrement button
	void Button_Rep(int in, char* D_com);						// Repeated button (continuity)
	void Button_Key(int in, char key[]);
	void Button_Chr(int in, int key);
	void Button_Mnu(int in, char* mnum); 
	void Button_RDf(int in, char* Dref, float Val);	
	boolean Set_Value(int in, char* D_com, float Val);				// Set specific value to dataref
	boolean Set_Val_R(int in, char* D_com, float Val);				// Set specific value to dataref +Repeat
	void Switch_Com(int in, char D_com1[], char D_com2[]);		// Toggle switch for two commands (0/1, on/off)
	void Switch_Dref(int in, char* Dref, float On, float Off);		// Toggle switch for dataref (btw two values))
// encoders
	float Encoder_Dref(int in, char* Dref, float val,  float inc, int type);		// Encoder reading for datarefs
	float Encoder_2xDref(int in, int btn, char* Dref, float val,  float inc1, float inc2, int type);
	void Encoder_Com(int in, char D_com1[], char D_com2[], int type); // Encoder reading for two commands
	void Encoder_2xCom(int in, int btn, char Com1[], char Com2[], char Com3[], char Com4[], int type);
// basic inputs
    void cmd_in (char Data_com[]); 
    void dref_in (char dref[], float Data); 
	void char_in (int key);
 	void key_in (char key_ch[]);
	void menu_in (char* mnum);  
 	void d_out (byte d_num);  
	int Bus_read(int in);  
	void Bus128(int pin1, int pin2, boolean inv);
	boolean Pin_status(int pin);
//	int get_slave(char type[], int row, int id); 
  private:
	int E_read(int e1, int e2);			// read 2 digital inputs(for Encoders)
	int E_read128(int in, int e2);		
	unsigned long d_time;
	unsigned long a_time;
    int XP_W;   
    int XP_R;  
	EthernetUDP Udp; 
    byte mac_ad[6];
	IPAddress ip;
    IPAddress ipx;  
	int A_range;
	int active;
	int A_pos[16];
	int state;   // current analog
	boolean In_mem [50];
//	byte Ext_dat[9];  
//	byte Ext_1[9]; 
//	byte Change;
    char* Data;  
    int packetSize;  
    int group;    
    int param;
    char DataRef[497];   
	byte Bus_in[16]; 
	byte Bus_mem[16]; 
    union { char bytevalue[4]; float floatvalue;}  bf;

};
#endif
