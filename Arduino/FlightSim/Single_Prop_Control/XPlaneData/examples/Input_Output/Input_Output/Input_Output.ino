/*
I/O interface for X-Plane Cockpit Simulator
using XPlaneData library

Receive/Send DATA from/to X-Plane sample code

( to receive all data check Data Groups ##37, 13, 114  
in "DATA Input & Output" menu of X-Plane) 

Vlad Sychev, 2014
http://svglobe.com
 */

#include <SPI.h>         
#include <Ethernet.h>
#include <XPlaneData.h>         // -- include XPlaneData library
//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8, 0};                     // X-Plane IP address ( ! set your !)
//IPAddress ip;                                   // Arduino IP address (Auto)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)
//--------------------------------------------------
 // work variables
int RPM1;              // RPM current    
int RPM1_pre;          // RPM previous
float Flaps;
float Flaps_pre;
int Pitot;


//---------------- setup ---------------------------
void setup()  { 

   XP.begin();      //==! init XPlaneData 
  
  XP.Pins_In (0, 12);       // Define pins for inputs   
 
 pinMode(13, OUTPUT);         // set output for LED
 

 Serial.begin(9600); 
   } 

//-----------------
void loop() 
{        
        // --- to control datarefs use Set_Value function 
       //  example of 3-position flaps kever on 3 digital inputs 5,6,7:
        
XP.Set_Value(5, "cockpit2/controls/flap_ratio[0]", 0);      // Flaps APR position                                 
XP.Set_Value(6, "cockpit2/controls/flap_ratio[0]", 0.5);     // Flaps APR position 
XP.Set_Value(7, "cockpit2/controls/flap_ratio[0]", 1 );      // Flaps Full position 
               
               //- Switch (for two commands)  example, connect switch to pin #8:
               
XP.Switch_Com(8, "ice/pitot_heat0_on", "ice/pitot_heat0_off"); // Pitot heat on/off
               
                   
                    //--  Press  Key emulation example (function Button_Key), pin # 11 :
                    
XP.Button_Key (11, "g");    //  Send key "g"   (default Gear up/dn)           
                                   

//----------- Receive data from X-Plane -----------------

if (XP.udpin()) 
    {                 // - define group number and position in this group to get needed parameter
  
  RPM1 = XP.Dget(37,1);  
              if (RPM1 != RPM1_pre) {   //  check if value has ben changed
              RPM1_pre = RPM1; 
               Serial.print("RPM1 = ");  Serial.println(RPM1); } 
   

 Flaps = XP.Dget(13, 5);
               if (Flaps_pre != Flaps) {  // check if value has ben changed
               Flaps_pre = Flaps; 
               Serial.print("Flaps = "); Serial.println(Flaps); }
               
 Pitot = XP.Dget(114, 5);      digitalWrite(13, Pitot);            // Pitot LED on/off 
           
    }



//--- end --------------------
 } 
 
//======== svglobe.com ================
