# -*- coding: utf-8 -*-
"""
Created on Mon Feb 16 10:38:36 2015

@author: crose
"""

import libpyzmq

def main ():
    
    # Initialise 0MQ with a single application and I/O thread
    ctx = libpyzmq.Context (1, 1)

    # Create a SUB socket ...
    s = libpyzmq.Socket (ctx, libpyzmq.SUB)

    # ... ask to subscribe to all messages ...
    s.setsockopt (libpyzmq.SUBSCRIBE , "")

    # ... request the tcp transport with the endpoint myserver.lan:5555
    s.connect ("tcp://myserver.lan:5555")

    while True:
        # Receive one message
        msg = s.recv ()

        # TODO: Process the message here
 
if __name__ == "__main__":
    main ()