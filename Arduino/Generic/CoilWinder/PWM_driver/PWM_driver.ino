/* RGB Analog Example, Teensyduino Tutorial #2
   http://www.pjrc.com/teensy/tutorial2.html

   This example code is in the public domain.
*/
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_PWMServoDriver.h"

LiquidCrystal_I2C lcd(0x27,16,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
Adafruit_StepperMotor *myMotor = AFMS.getStepper(200, 1);

const int winderPin =  2;    //used to drive the motor PWM
const int countPin = 3;      //used for the turn counter
const int turnCountLimitPin = 4; //this pin is used for a switch to allow the user to enter max turns
int counterState = 0;
int counterStateLast = HIGH; //used for debounce
int windingCount=0;
int turnCountLimit=0;

void setup()   {                
  pinMode(winderPin, OUTPUT);
  pinMode(countPin, INPUT);
  pinMode(countPin,INPUT_PULLUP);
  lcd.init();                      // initialize the lcd 
 
  // Print a message to the LCD.
  lcd.backlight();
  
  lcd.setCursor(0,0);
  lcd.print("Arduino Coil Winder");
  lcd.setCursor(0,1);
  lcd.print("Count:");
  lcd.print(windingCount);  

}

void loop()                     
{
  
  analogWrite(winderPin, 180);
  delay(10);
 
 // read the state of the counter switch value:
  counterState = digitalRead(countPin);

  // check if the pushbutton is pressed.
  // if it is, the buttonState is HIGH:
  if (counterState == LOW) {  
   if (counterStateLast != LOW) {  //dont count again until last state is high 
  
      windingCount++;  
      lcd.setCursor(0,1);
      lcd.print("Count:");
      lcd.print(windingCount);  
   }
  counterStateLast=LOW;
  } else {
    counterStateLast=HIGH;
  }
}
