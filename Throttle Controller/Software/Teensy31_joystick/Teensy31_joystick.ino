/* Complete USB Joystick Example
   Teensy becomes a USB joystick with 16 or 32 buttons and 6 axis input

   You must select Joystick from the "Tools > USB Type" menu

   Pushbuttons should be connected between the digital pins and ground.
   Potentiometers should be connected to analog inputs 0 to 5.

   This example code is in the public domain.
*/

// Configure the number of buttons.  Be careful not
// to use a pin for both a digital button and analog
// axis.  The pullup resistor will interfere with
// the analog voltage.
const int numButtons = 32;  // 16 for Teensy, 32 for Teensy++
const int numDigitalInputs = 13; //the electrical design has 13 digital inputs. We will create one joy button for OFF and one joy button for ON for each input
const int numAnalogInputs = 9;
const int DigitalPins[numDigitalInputs] = {0,1,2,3,4,5,6,7,8,9,10,11,12};
const int AnalogPins[numAnalogInputs] = {0,1,2,3,6,7,8,9,10};

void setup() {
  // you can print to the serial monitor while the joystick is active!
  Serial.begin(9600);
  // configure the joystick to manual send mode.  This gives precise
  // control over when the computer receives updates, but it does
  // require you to manually call Joystick.send_now().
  Joystick.useManualSend(true);
  for (int i=0; i<numButtons; i++) {
    pinMode(i, INPUT_PULLUP);
  }
  //Serial.println("Begin Complete Joystick Test");
}

byte allButtons[numButtons];
byte prevButtons[numButtons];
int angle=0;

void loop() {
  // read 9 analog inputs and use them for the joystick axis
  Joystick.X(analogRead(AnalogPins[0]));
  Joystick.Y(analogRead(AnalogPins[1]));
  Joystick.Z(analogRead(AnalogPins[2]));
  //Joystick.Xrotate(0);
  //Joystick.Yrotate(0);
  Joystick.Zrotate(0);
  Joystick.sliderLeft(0);
  Joystick.sliderRight(0);
  Joystick.slider(0);

//  Joystick.Zrotate(analogRead(AnalogPins[3]));
//  Joystick.sliderLeft(analogRead(AnalogPins[4]));
//  Joystick.sliderRight(analogRead(AnalogPins[5]));
//  Joystick.slider(analogRead(AnalogPins[6]));

  Joystick.send_now(); 

  // read digital pins and use them for the buttons
  //Since FSX works best with button pushes and not toggle switches.
  //I split the button range into a hi/low side of the toggle switches.
  for (int i=0; i<numDigitalInputs; i++) {
    if (digitalRead(DigitalPins[i])) {
      // when a pin reads high, the button is not pressed
      // the pullup resistor creates the "on" signal
//      allButtons[2*i] = 0;
//      allButtons[2*i+1]=1;

      Joystick.button(2*i+1,0);
      Joystick.button(2*i+2,1);      

    } else {
      // when a pin reads low, the button is connecting to ground.
//      allButtons[2*i] = 1;
//      allButtons[2*i+1]=0;

      Joystick.button(2*i+1,1);
      Joystick.button(2*i+2,0);
      
    }
//    Joystick.button(2*i + 1, allButtons[2*i]);
//    Joystick.button(2*i + 2, allButtons[2*i+1]);
  }

  // make the hat switch automatically move in a circle
  /*
  angle = angle + 1;
  if (angle >= 360) angle = 0;
  Joystick.hat(angle);
  */
 
  // check to see if any button changed since last time
//  boolean anyChange = false;
//  for (int i=0; i<numButtons; i++) {
//    if (allButtons[i] != prevButtons[i]){
//      anyChange = true;
//      Joystick.button(2*i + 1, allButtons[2*i]);
//      Joystick.button(2*i + 2, allButtons[2*i+1]);
//    }
//    prevButtons[i] = allButtons[i];
//  }
  
  // if any button changed, print them to the serial monitor
//  if (anyChange) {
//    Joystick.send_now(); 
//    Serial.print("Buttons: ");
//    for (int i=0; i<numButtons; i++) {
//      Serial.print(allButtons[i], DEC);
//    }
//    Serial.println();
//  }
  
  // Because setup configured the Joystick manual send,
  // the computer does not see any of the changes yet.
  // This send_now() transmits everything all at once.
  
  Joystick.send_now();  
  // a brief delay, so this runs "only" 200 times per second
  delay(5);
}

