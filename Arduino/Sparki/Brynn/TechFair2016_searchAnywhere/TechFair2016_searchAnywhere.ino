/*******************************************
 Sparki - a program to track a line and pick up any object found on the line
 Brynn Rose - 12/23/2015
*********************************************/

#include <Sparki.h> // include the sparki library
enum STATES {search=0, pickup=1, carry=2, dropoff=3, lostline = 4};
STATES state;

#define threshold_dark 600
#define threshold_white 800
void setup() 
{ sparki.gripperOpen();
  delay(6000);
  sparki.gripperStop();
  state = lostline;
  sparki.servo(SERVO_CENTER); // puts head in the center
   
}

void loop() {

  int lineLeft   = sparki.lineLeft();   // create a variable called "lineLeft" and set it equal to the measured value from the left IR sensor
  int lineCenter = sparki.lineCenter(); // same as above except using the center IR sensor
  int lineRight  = sparki.lineRight();  // same as above except using the right IR sensor
  int target_distance_cm = sparki.ping(); // measures the distance with Sparki's eyes   
       
  switch(state){
    
    case lostline:
        look_for_line(1);
        state = search;
        break;
        
    case search:  //do line tracking   
  
      if ( lineCenter < threshold_dark ) // if line is below left line sensor
      {  
        sparki.moveForward(); // move forward
      }
      else{
        if ( lineLeft < threshold_dark ) // if line is below left line sensor
        {  
          sparki.moveLeft(); // turn left
        }
      
        if ( lineRight < threshold_dark ) // if line is below right line sensor
        {  
          sparki.moveRight(); // turn right
        }
      }

      //look for white line. It means we are at the edge of the board without finding something to carry. So turn around and keep searching
      if ( (lineCenter > threshold_white) && (lineLeft > threshold_white) && (lineRight > threshold_white) ) {
          look_for_line(0);
      }

      if (target_distance_cm != -1) {  //if distance sensor got a good measurement
        if (target_distance_cm <= 2 ) {  //then see if distance is less than 2cm
          state = pickup;              //if so the go to the pickup state
        }
      }  
      break;

    case carry:  //do line tracking   
  
      if ( lineCenter < threshold_dark ) // if line is below left line sensor
      {  
        sparki.moveForward(); // move forward
      }
      else{
        if ( lineLeft < threshold_dark ) // if line is below left line sensor
        {  
          sparki.moveLeft(); // turn left
        }
      
        if ( lineRight < threshold_dark ) // if line is below right line sensor
        {  
          sparki.moveRight(); // turn right
        }
      }

      //All three censors are over the white line
      if ( (lineCenter > threshold_white) && (lineLeft > threshold_white) && (lineRight > threshold_white) ) {
        state = dropoff;  
      }
      
      break;      
      

    case pickup:
       sparki.moveStop();
       sparki.gripperClose();  // open the robot's gripper
       delay(6000);           // for 4 second (4000 milliseconds)
       sparki.gripperStop();
       state = carry;
       break;
       
    case dropoff:
      sparki.moveStop();
      sparki.gripperOpen();
      delay(6000);
      sparki.gripperStop();
      sparki.moveBackward(5);
      sparki.moveRight(180);
      state = search;
      break;
  }

  sparki.clearLCD(); // wipe the screen
  
  sparki.print("Line Left: "); // show left line sensor on screen
  sparki.println(lineLeft);
  
  sparki.print("Line Center: "); // show center line sensor on screen
  sparki.println(lineCenter);
  
  sparki.print("Line Right: "); // show right line sensor on screen
  sparki.println(lineRight);
  
  sparki.print("State: ");
  sparki.println(state);
  sparki.print("Distance: ");
  sparki.println(target_distance_cm);
  
  sparki.updateLCD(); // display all of the information written to the screen

  delay(10); // wait 0.01 seconds
}

void look_for_line(int moveDistance) {
  boolean linefound = false; 
  while ( ! linefound  ) {
    sparki.moveRight(4);
    sparki.moveForward(moveDistance);
    int lineRight  = sparki.lineRight();  // same as above except using the right IR sensor
    int lineLeft = sparki.lineLeft (); 
    int lineCenter = sparki.lineCenter();
    
    if (lineLeft < threshold_dark ) linefound = true;
    if (lineCenter < threshold_dark ) linefound = true;
    if ( lineRight < threshold_dark ) linefound = true;
    }  
}
  


