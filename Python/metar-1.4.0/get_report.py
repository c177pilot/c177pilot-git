#!/usr/bin/python
#
import os
import sys
import getopt
#import argparse
import string
import urllib
from metar import Metar

#BASE_URL = "http://weather.noaa.gov/pub/data/observations/metar/stations"
#the weather.noaa website is no longer available.

BASE_URL = "https://www.aviationweather.gov/metar/data/"
#Note: must add to base URL "?ids=KLZU+KPDK+KATL&format=raw&date=0&hours=0"
#stations = ["KLZU","KPDK"]
debug = False

def usage():
  program = os.path.basename(sys.argv[0])
  print "Usage: ",program,"<station> [ <station> ... ]"
  print """Options:
  <station> . a four-letter ICAO station code (e.g., "KEWR")
"""
  sys.exit(1)

def main(stations):
      
    for name in stations:
#      url = "%s" % (BASE_URL+"/?ids=", name)
#      url = "https://www.aviationweather.gov/metar/data/?ids={0!s}".format(name)
      url = BASE_URL+"?ids={0!s}".format(name)
      #print url
      if debug: 
        sys.stderr.write("[ "+url+" ]")
      try:
        urlh = urllib.urlopen(url)
        #print urlh
        report = ''
        for line in urlh:
#            """this is currently very inefficient,  the urlh contains
#            a ton of HTML prior to the coded METAR report, the line prior to
#            the METAR is this:
#                <b>Data at: 2037 UTC 28 Sep 2016</b></p>
#                <!-- Data starts here -->
#            """
        
          #print line  
          if line.startswith(name):
            report = line.strip()
            obs = Metar.Metar(line)
            #print obs.string()
            print obs.code
            print obs.sky_conditions()
            print obs.sky_data()
            print obs.wind_dir()
            print obs.wind_speed()
            print obs.runway()
            break
        if not report:
          print "No data for ",name,"\n\n"
      except Metar.ParserError, err:
        print "METAR code: ",line
        print string.join(err.args,", "),"\n"
      except:
        print "Error retrieving",name,"data","\n"
 
if __name__ == "__main__":

    try:
      opts, stations = getopt.getopt(sys.argv[1:], 'd')
      print opts
      for opt in opts:
        if opt[0] == '-d':
          debug = True
    except:
      usage()
      
    if not stations:
      usage()
      exit()
    else:
      main(stations)