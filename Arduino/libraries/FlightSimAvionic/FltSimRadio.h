#ifndef FLTSIMRADIOINTERFACE_H
#define FLTSIMRADIOINTERFACE_H
#include "FLTSIMRADIOINTERFACE.h"
#include <Encoder.h>
#include <Bounce.h>

/*
  FltSimRadio
  created 29-January-2016 V 0.1
  By Chris Rose
*/

class fltSimRadio : public fltSimRadioInterface
{
	private:
	// Bounce ComFlipFlopSwitch = Bounce(COMFLIPFLOPSWITCHPIN, DEBOUNCETIME);
	// Bounce NavFlipFlopSwitch = Bounce(NAVFLIPFLOPSWITCHPIN, DEBOUNCETIME);
	// Encoder EncNavCoarse = Encoder(ENCNAVCOARSEPIN_A,ENCNAVCOARSEPIN_B);// Rotary Encoder A channel pin and B channel pin
	// Encoder EncNavFine = Encoder(ENCNAVFINEPIN_A,ENCNAVFINEPIN_B);
	// Encoder EncComCoarse = Encoder(ENCCOMCOARSEPIN_A,ENCCOMCOARSEPIN_B);
	// Encoder EncComFine = Encoder(ENCCOMFINEPIN_A,ENCCOMFINEPIN_B);
	// FlightSimCommand ComFlipFlop[2], NavFlipFlop[2];
	// FlightSimInteger NavFreq_stdby_Mhz[2], NavFreq_stdby_khz[2];
	// FlightSimInteger ComFreq_stdby_Mhz[2], ComFreq_stdby_khz[2];
	// FlightSimInteger NavFreq_primary_Mhz[2], NavFreq_primary_khz[2];
	// FlightSimInteger ComFreq_primary_Mhz[2], ComFreq_primary_khz[2];
	// FlightSimInteger *ComFreq_Mhz[2], *ComFreq_khz[2], *NavFreq_Mhz[2], *NavFreq_khz[2];
	
	public:
	//pass encoders as [Coarse_Enc, Fine_Enc] array
	//fltSimRadio(int NavComNum,Encoder* ComEnc[2],Encoder* NavEnc[2], Bounce* ComFlipFlopSwitch, Bounce* NavFlipFlopSwitch, boolean flipflop);
	//run();
	
};
#endif