/*
I/O interface for X-Plane Cockpit Simulator
using XPlaneData library

Receive/Send DATA from/to X-Plane sample code
3-position Flaps switch 

( to receive all data check Data Groups # 13 
in "DATA Input & Output" menu of X-Plane) 

Vlad Sychev, 2014
http://svglobe.com
 */

#include <SPI.h>         
#include <Ethernet.h>
#include <Wire.h>
#include <XPlaneData.h>         // -- include XPlaneData library
#include <Servo.h> 
//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8, 0};                     // X-Plane IP address ( ! set your !)
//IPAddress ip;                                   // Arduino IP address (Auto)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)
//--------------------------------------------------
 
Servo Flaps_Pos;        //---- Flaps servo

float Flaps;      // Flaps, current
float Flaps_pre;  // Flaps previous

//---------------- setup ---------------------------
void setup()  { 

   XP.begin();      //==! init XPlaneData 
  XP.Pins_In (5, 7);       // Define pins for inputs  
 
  
Flaps_Pos.attach(2);                              
Flaps_Pos.writeMicroseconds(1160); 

   } 
           
         
           
//------------------------ start --------------------
void loop() 
{                // ------ send data to X-Plane -----
        // --- to control datarefs use Set_Value function 
       //  example of 3-position flaps kever on 3 digital inputs 5,6,7:
        
XP.Set_Value(5, "cockpit2/controls/flap_ratio[0]", 0);      // Flaps APR position                                 
XP.Set_Value(6, "cockpit2/controls/flap_ratio[0]", 0.5);     // Flaps APR position 
XP.Set_Value(7, "cockpit2/controls/flap_ratio[0]", 1 );      // Flaps Full position 

//----------- Receive data from X-Plane -----------------

if (XP.udpin())   // - must 
    {                 // - define group number (13) and position in this group (5) to get Flaps position
 
 Flaps = XP.Dget(13, 5);
         Flaps = int(Flaps*100);

               if (Flaps_pre != Flaps) {      // check if value has ben changed
                   Flaps_pre = Flaps; 
                   Flaps = map(Flaps, 0, 100, 800, 2000);                       // calibrate mapping value for your servo!!
                Flaps_Pos.writeMicroseconds(Flaps); 
           
    }

}

} 

