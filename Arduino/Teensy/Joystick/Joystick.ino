/* Complete USB Joystick Example
   Teensy becomes a USB joystick with 16 or 32 buttons and 6 axis input

   You must select Joystick from the "Tools > USB Type" menu

   Pushbuttons should be connected between the digital pins and ground.
   Potentiometers should be connected to analog inputs 0 to 5.

   This example code is in the public domain.
*/
#include <Wire.h>
#include <Adafruit_MCP23017.h>  
#include <ADC.h>
#include "adcSwitch.h"

#define FLAP_POSITIONS 4 // Cessna type: 0,10,20,30. This must match the physical number of positions for the flaps switch
#define STARTER_POSITIONS 5 //0=off, 1=left mag, 2=right mag, 3=both, 4=start
#define RADIO_TUNE_SELECT_POSITIONS 4 //NAVCOM1 = 0, NAVCOM2 = 1, XPONDER = 2, GPS = 3
#define NUM_ON_CHIP_BUTTONS 9

// Configure the number of buttons.  Be careful not
// to use a pin for both a digital button and analog
// axis.  The pullup resistor will interfere with
// the analog voltage.
const int numButtons = 16;  // 16 for Teensy, 32 for Teensy++

//define pins used for analog inputs
const int RADIOTUNEPOTPIN = A7;
const int FLAPRQSTPOTPIN = A8;
const int ELEVTRIMPOTPIN = A6;
const int STARTERPOTPIN = A9;

//create the Teensy ADC object that will be used by the analog switch axes
ADC *adc = new ADC();

adcSwitch radioTuneADCSwitch = adcSwitch(adc, RADIOTUNEPOTPIN, RADIO_TUNE_SELECT_POSITIONS);
adcSwitch flapRqstADCSwitch = adcSwitch(adc, FLAPRQSTPOTPIN, FLAP_POSITIONS);
adcSwitch starterADCSwitch = adcSwitch(adc, STARTERPOTPIN, STARTER_POSITIONS);

void setup() {
  // you can print to the serial monitor while the joystick is active!
  Serial.begin(9600);
  // configure the joystick to manual send mode.  This gives precise
  // control over when the computer receives updates, but it does
  // require you to manually call Joystick.send_now().
  Joystick.useManualSend(true);
  for (int i=0; i<numButtons; i++) {
    pinMode(i, INPUT_PULLUP);
  }
  Serial.println("Begin Complete Joystick Test");
}

byte allButtons[numButtons];
byte prevButtons[numButtons];
int angle=0;

void loop() {
  int FlapRqst = flapRqstADCSwitch.value();
  float ElevTrim = (float)2*adc->analogRead(ELEVTRIMPOTPIN)/65536 - 1;
  
  // read 6 analog inputs and use them for the joystick axis
  Joystick.X(ElevTrim/*analogRead(0)*/);
  Joystick.Y(FlapRqst/*analogRead(1)*/);
  Joystick.Z(analogRead(2));
  Joystick.Zrotate(analogRead(3));
  Joystick.sliderLeft(analogRead(4));
  Joystick.sliderRight(analogRead(5));
  
  // read digital pins and use them for the buttons
  for (int i=0; i<numButtons; i++) {
    if (digitalRead(i)) {
      // when a pin reads high, the button is not pressed
      // the pullup resistor creates the "on" signal
      allButtons[i] = 0;
    } else {
      // when a pin reads low, the button is connecting to ground.
      allButtons[i] = 1;
    }
    Joystick.button(i + 1, allButtons[i]);
  }

  // make the hat switch automatically move in a circle
  //angle = angle + 1;
  //if (angle >= 360) angle = 0;
  Joystick.hat(angle);
  
  // Because setup configured the Joystick manual send,
  // the computer does not see any of the changes yet.
  // This send_now() transmits everything all at once.
  Joystick.send_now();
  
  // check to see if any button changed since last time
  boolean anyChange = false;
  for (int i=0; i<numButtons; i++) {
    if (allButtons[i] != prevButtons[i]) anyChange = true;
    prevButtons[i] = allButtons[i];
  }
  
  // if any button changed, print them to the serial monitor
  if (anyChange) {
    Serial.print("Buttons: ");
    for (int i=0; i<numButtons; i++) {
      Serial.print(allButtons[i], DEC);
    }
    Serial.println();
  }
  
  // a brief delay, so this runs "only" 200 times per second
  delay(5);
}

