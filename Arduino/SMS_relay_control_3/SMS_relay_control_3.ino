/*
 * SMS_relay_control ver 3.0
 * Chris Rose - PlanesGeorgia
 * 29-January-2017
 * 
*/
//#define TESTMODE

#ifndef TESTMODE
#include <GPRS_Shield_Arduino.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#endif

// Use the Internal Arduino EEPROM as storage
#include <EEPROM.h>

#define PIN_TX    7
#define PIN_RX    8
#define BAUDRATE  9600
#define TICK 1000 /* should be a 1-second tick (delay) in main loop for alarm timing calculation */
#define TICKALARM 86400 /*24 hours*/
#define Nrelay 2 /*number of relays under control - sendSMS_state() and smsHelp{""} will need modified if this is ever > 2 */

#define ON false
#define OFF true

int relayPin[Nrelay] = {2,3};

#define MESSAGE_LENGTH 160
char msgIn[MESSAGE_LENGTH];
//char msgOut[MESSAGE_LENGTH];

char msgHelp[MESSAGE_LENGTH] =  {
  "Usage: ?=help, ??=status\n \
  @1,@2,@12 ON\n \
  !1,!2,!12 OFF\n \
  $1,$2,$12 (re)SET ON-24hr"};

int messageIndex = 0;

//Number of authorized phones to control the system
#define Nphone 4
//char authPhone[Nphone][16] = { "+17704035517", "+14044027687", "", ""}; //64-bytes
char authPhone[Nphone][16] = { "", "", "", ""}; //will act on SMS from all phone's if this list blank, will not send bootup SMS broadcast to empty numbers 

char phone[16]; //global to hold phone number of incoming message
char datetime[24];

typedef struct {
  bool relay[2] = {OFF, OFF};
  unsigned long tick[2] = {0,0};
  float temp[2] = {0,0};
  int tickInc[2] = {0,0};
} struct_State;

struct_State state;
  
#ifndef TESTMODE
  GPRS gprsTest(PIN_TX,PIN_RX,BAUDRATE);//RX,TX,PWR,BaudRate
#endif
  
#define Nmsg 11
char msgSet[Nmsg][5] = { /*if the order of these array is altered it may require change to dispatchMessage() */
    "?",    /*help, will send message of this command set*/
    "??",   /*returns status report, relay stats, remaining shutoff time, temps*/
    "@1",   /*turn on #1 */
    "@2",   /*turn on #2 */
    "@12",  /*turn on #1 & #2 */
    "!1",   /*turn off #1 */
    "!2",   /*turn off #2 */
    "!12",  /*turn off #1 & #2 */
    "$1",   /*turn on #1 for 24hrs, SMS sent when auto-off occurs */
    "$2",   /*turn on #2 for 24hrs, SMS sent when auto-off occurs */
    "$12"   /*turns on #1 and #2 for 24hrs, SMS sent when auto-off occurs */
};

void dispatchMsg(int msgNum) { //'phone' is a global variable and will hold number of most recent SMS source
  
  if (msgNum > 1) { //every message after the help query and status query will reset the alarms, so do that logic here
    Serial.println("Resetting counters");
    for (int i=0;i<Nrelay;i++) {
      state.tick[i]=0;
      state.tickInc[i]=0;
    }
  }
  switch (msgNum) {
    case 0: /*? - the help message*/ { sendSMS(msgHelp); break; } 
    case 1: /*?? - request status report*/ { break; }
    case 2: /*@1 - turn on #1*/ { state.relay[0]=ON; break; }
    case 3: /*@2 - turn on #2*/ { state.relay[1]=ON; break; }
    case 4: /*@12 - turn on #1&#2*/ { state.relay[0]=ON; state.relay[1]=ON; break; }
    case 5: /*!1 - turn off #1*/ { state.relay[0]=OFF; break;}
    case 6: /*!2 - turn off #2*/ { state.relay[1]=OFF; break;}
    case 7: /*!12 - turn off #1&#2*/ {state.relay[0]=OFF; state.relay[1]=OFF; break;}
    case 8: /*$1 - turn on #1 24hrs*/ { state.relay[0]=ON; state.tickInc[0]=1; break;}
    case 9: /*$2 - turn on #2 24hrs*/ { state.relay[1]=ON; state.tickInc[1]=1; break;}
    case 10: /*$12 - 24hr on #1&#2*/ { state.relay[0]=ON; state.relay[1]=ON; state.tickInc[0]=1; state.tickInc[1]=1;  break;}                                   
  }
  
  if (msgNum > 0) { //every message after the help query will echo back an SMS status message
    sendSMS_state();
  }
}

void powerUp() {
 pinMode(9, OUTPUT);
 digitalWrite(9,LOW);
 delay(1000);
 digitalWrite(9,HIGH);
 delay(2000);
 digitalWrite(9,LOW);
 delay(3000);
}

void setup() {
  Serial.begin(9600);
  for (int i=0;i<Nrelay;i++) {
    pinMode(relayPin[i],OUTPUT);
    digitalWrite(relayPin[i],OFF); //set all pins off as they are initialized
  }
  //dispatchMsg(7); //set the relay state structure to all off
  
  powerUp();  //execute the power up sequence on pin 9
  
  #ifndef TESTMODE
    while(!gprsTest.init()) {
        Serial.print("init error\r\n");
        delay(1000);
    }
    delay(3000);  
    Serial.println("Init Success, please send SMS message to me!");

  #endif
  #ifdef TESTMODE
    dispatchMsg(1);
  #endif

  sendSMS_all((char*)"PlanesGeorgia SMS relay controller just booted. Now ready for commands. Send ? for help.");
}

void sendSMS_state() {
  char stateMsg[MESSAGE_LENGTH];
  sprintf(stateMsg,"#1:%01d,#2:%01d,CLK1:%02d,CLK2:%02d,T1:NA,T2:NA",!state.relay[0],!state.relay[1],(int)(state.tick[0])/3600,(int)(state.tick[1])/3600);
  sendSMS(stateMsg);
}

void sendSMS_all(char* msgSMS) {
  for (int i=0;i<Nphone;i++) {
    if (strcmp(authPhone[i],"")) { //if an authPhone element is NOT blank then send an SMS to it.
      #ifndef TESTMODE
        gprsTest.sendSMS(authPhone[i], msgSMS);
      #else
        Serial.println(msgSMS);
        Serial.println("Known Number");
        Serial.println(authPhone[i]);
      #endif
    } else {
      Serial.println("Empty Phone Number");
      Serial.println(authPhone[i]);
    }
  }
}

void sendSMS(char* msgSMS) {
  #ifndef TESTMODE
    gprsTest.sendSMS(phone, msgSMS); //define phone number and text
  #else
    Serial.print(phone);
    Serial.print(": ");
    Serial.println(msgSMS);
  #endif
}

void setState() {
  for (int i=0;i<Nrelay;i++) {
    digitalWrite(relayPin[i],state.relay[i]);
  }
}

void loop() {

   bool phoneMatch = false;

   #ifndef TESTMODE
      messageIndex = gprsTest.isSMSunread();
   #else
      messageIndex = 1;
   #endif
   
   while (messageIndex > 0) {

      #ifndef TESTMODE
        //grab the message
        gprsTest.readSMS(messageIndex, msgIn, MESSAGE_LENGTH, phone, datetime);
        
        //In order not to fill SIM Memory, is better to delete it
        gprsTest.deleteSMS(messageIndex);
      
      #else
/*     Serial.println("Enter message and Press ENTER");
       while (Serial.available() == 0) { Serial.println("Enter message and Press ENTER"); delay(100);}  // Wait here until input buffer has a character

          //int incomingByte = Serial.read();
          //Serial.print("I received: ");
          //Serial.println(incomingByte, DEC);
          String string_in = Serial.readString();
          string_in.replace("\n","");
          string_in.toCharArray(msgIn,MESSAGE_LENGTH);  //for some reason the msgIn here does not pass the strcmp() as it does if strcpy() is used below.
          Serial.println(msgIn);
*/
          strcpy(msgIn,"??");
          strcpy(phone,"17704035517");
         
      #endif
      messageIndex-=1;

      Serial.print("From number: ");
      Serial.println(phone);  
      Serial.print("Datetime: ");
      Serial.println(datetime);        
      Serial.print("Received Message: ");
      Serial.println(msgIn);
      
      //check the phone against authorized numbers (phoneMatch == True)
      bool phoneListEmpty=true;
      for (int i=0;i<Nphone;i++) {
        if (strcmp(authPhone[i],"")) { //if an authPhone element is not blank
          phoneListEmpty = false;
          //Serial.println("Phone list is NOT empty");
        }
      }

      if (phoneListEmpty == false) { //if the phone list is not empty, then check for valid numbers
        for (int i=0;i<Nphone;i++) {
          if (!strcmp(authPhone[i],phone)) {
            phoneMatch = true;
            //Serial.println("Phone number match");
            //Serial.println(phone);
          }
        } //end for-loop
      } else {
        phoneMatch = true;  //force phoneMatch if the authPhone array is empty
      } //end if-else

      if (phoneMatch == true) {
        

        
        //dispatch the message if authorized user
        bool msgSetMatch = false;
        for (int i=0; i<Nmsg; i++) {
          if (!strcmp(msgSet[i],msgIn)) {
            dispatchMsg(i);
            msgSetMatch=true;
          } 
        } //end for
        if (!msgSetMatch) {
          sendSMS((char*)"Invalid Command"); 
        } //end if
      } else {
          sendSMS((char*)"Unauthorized User");
     } //end if-else

     //process timers
     for(int i=0; i<Nrelay; i++) {
      state.tick[i] += state.tickInc[i]; //tickInc will be 1 if alarm is set, otherwise 0;
      if (state.tick[i] > TICKALARM ) { //if alarm has fired
        state.relay[i] = false; //turn off relay
        state.tickInc[i] = 0; //cancel alarm incrementing
        state.tick[i] = 0;  //set alarm count to zero
      }
      //Serial.println((state.tick[i]));
     }

   } //end while()

   setState(); //set the relay IO to current state
   Serial.println("I'm alive!");  

   delay(TICK);
}


