#ifndef ADCSWITCH_H
#define ADCSWITCH_H
#include "ADC.h"


/*
  ADCSwitch,
  created 31-December-2015 V 0.1
  By Chris Rose
*/

class adcSwitch {
private:  
  
  // AnalogPin
  int m_pin =0;
  int m_throw =1;	//the number of switch positions (commonly called throw)
  int m_adcMax = 1023;
  ADC *m_adc;
  
  // Status of last value
  int m_lastValue = 0;
  bool m_isChanged = true;
  
public:
  adcSwitch(ADC *adc, int pin, int nthrow);
  bool update();	//returns true if the value has changed since last call, otherwise false.
  int value();	//returns the value of the analog Axis Switch

};

#endif