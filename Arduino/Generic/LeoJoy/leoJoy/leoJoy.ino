/*
Arduino Leonardo Joystick!
*/


JoyState_t joySt;

int ledPin = 13;
int dither = 1;

void setup()
{
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600); 
	joySt.xAxis = 0;
	joySt.yAxis = 0;
	joySt.zAxis = 0;
	joySt.xRotAxis = 0;
	joySt.yRotAxis = 0;
	joySt.zRotAxis = 0;
	joySt.throttle = 0;
	joySt.rudder = 0;
	joySt.hatSw1 = 0;
	joySt.hatSw2 = 0;
	joySt.buttons = 0;

}


void loop()
{
	joySt.xAxis = 0;
	joySt.yAxis = 0;
	joySt.zAxis = 0;
	joySt.xRotAxis = 0;
	joySt.yRotAxis = 0;
	joySt.zRotAxis = 0;
	joySt.throttle = 0;
	joySt.rudder = 0;
	joySt.hatSw1 = 0;
	joySt.hatSw2 = 0;
	joySt.buttons = 0;
 

	joySt.zAxis += analogRead(0);
	joySt.yAxis += analogRead(1);
	joySt.xAxis += analogRead(2);
	joySt.xRotAxis += analogRead(3);
	joySt.yRotAxis += analogRead(4);
	joySt.zRotAxis += analogRead(5);
	joySt.throttle += analogRead(6); 
	joySt.rudder += analogRead(7);
     
    //add +/- one ADC count each time through the loop. This should be beyond the bandwidth of the X-plane model to respond
/*    dither = -dither;  //simply toggle the dither between +/- 1
    joySt.xAxis += dither;
    joySt.yAxis += dither;
    joySt.zAxis += dither;
    joySt.xRotAxis += dither;
    joySt.yRotAxis += dither;
    joySt.zRotAxis += dither;
    joySt.throttle += dither;
    joySt.rudder += dither;
*/

/*
//      No assigment of pins to the hardware but manipulate the data structure
	joySt.buttons <<= 1;
	if (joySt.buttons == 0)
		joySt.buttons = 1;
*/

/*
//      No assignment of hat switch hardware
	joySt.hatSw1++;
	joySt.hatSw2--;
*/

/*
	if (joySt.hatSw1 > 8)
		joySt.hatSw1 = 0;
	if (joySt.hatSw2 > 8)
		joySt.hatSw2 = 8;
*/
      //Serial.print(joySt.xAxis);
      //Serial.print("\n");      
    
	if (joySt.xAxis > 127)  //this structure is a 8-bit (512 maximum)
		digitalWrite(ledPin, HIGH);
	else
		digitalWrite(ledPin, LOW);


	// Call Joystick.move
	Joystick.setState(&joySt);
	delay(100);  

}
