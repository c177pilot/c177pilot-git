/* 
This is a test sketch for the Adafruit assembled Motor Shield for Arduino v2
It won't work with v1.x motor shields! Only for the v2's with built in PWM
control

For use with the Adafruit Motor Shield v2 
---->	http://www.adafruit.com/products/1438
*/


#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_PWMServoDriver.h"

LiquidCrystal_I2C lcd(0x27,16,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display
// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Connect a stepper motor with 200 steps per revolution (1.8 degree)
// to motor port #2 (M3 and M4)
Adafruit_StepperMotor *myMotor = AFMS.getStepper(200, 2);

int turnCount = 16;

void setup() {
  lcd.init();                      // initialize the lcd 
  lcd.clear();
  lcd.backlight();
  lcd.print("Stepper test!");

  //AFMS.begin();  // create with the default frequency 1.6KHz
  AFMS.begin(1600);  // OR with a different frequency, say 1KHz
  
  myMotor->setSpeed(10);  // 10 rpm   
  lcd.setCursor(0,0);
  lcd.print(turnCount);
  lcd.print(": Turns");
  myMotor->step(turnCount*200, FORWARD, SINGLE); 
  lcd.setCursor(0,0);
  lcd.print("Done!");
}

void loop() {

  //myMotor->step(turnCount*200, BACKWARD, SINGLE); 
  //myMotor->step(FORWARD, SINGLE);
  //delay(10);
}
