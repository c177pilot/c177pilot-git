/*
Interface for Baron 58 Simulator

Example for Gear Up/Down with 6 LEDs indication
for XPlaneData library

Vlad
http://svglobe.com
 */

#include <SPI.h>         
#include <Ethernet.h>
#include <XPlaneData.h>         // -- include XPlaneData library
//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8, 0};                     // X-Plane IP address ( ! set your !)
//IPAddress ip;                                   // Arduino IP address (Auto)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)
//--------------------------------------------------

float Gear1 = 1;                //--- Gear #1 position ( 1.00 - Down&Locked, 0 - Up)
float Gear2 = 1;                //--- Gear #2 position 
float Gear3 = 1;                //--- Gear #3 position 



 // --- Pins for LEDs   (For this sample, actually I use one serial digital output for all leds) 
   int GearPos1 = 6; 
   int GearTrans1 = 7;      //- Pin for Led "In Transit"     
   int GearPos2 = 8;
   int GearTrans2 = 9;   
   int GearPos3 = 11;
   int GearTrans3 = 12;
   
//--------------

void setup()
      {
   XP.begin();      //==! init XPlaneData 

  XP.Pins_In (2, 3);       // Define pins for inputs (first, last including)  2-3   
  XP.Pins_Out(6, 12);   // Define pins for outputs (first, last including) Pins# 6-12   


      }
//***************** Main program loop *************************************

void loop() 

{

   // --Gear Up/Dn example -  gear_lever on Pins 2 and 3 Arduino  

XP.Button_Cmd(2, "flight_controls/landing_gear_up");  
XP.Button_Cmd(3, "flight_controls/landing_gear_down"); 
                  
  

//================================ RECEIVING   DATA   FROM X-PLANE ==================================================

if (XP.udpin())   // - !! All "Dget" requests must be inside this 
    {              
       // -  define group number (67) and position in this group (1,2,3) to get gear positions
       // use the XP object  with "Dget" function to get any other Data, with two arguments
       //  - the first number is group number, second is position of the parameter in this group
       
 //--------- Gear Retraction Positions  (0-1 )-------------------

          Gear1 = XP.Dget(67, 1);
          Gear2 = XP.Dget(67, 2); 
          Gear3 = XP.Dget(67, 3);              
           
                 if (Gear1 > 0 && Gear1 < 1)   digitalWrite(GearTrans1, HIGH); else digitalWrite(GearTrans1, LOW);            
                 if (Gear1 == 1) { digitalWrite(GearTrans1, LOW); digitalWrite(GearPos1, HIGH); }  else digitalWrite(GearPos1, LOW);
                 if (Gear2 > 0 && Gear2 < 1)   digitalWrite(GearTrans2, HIGH); else digitalWrite(GearTrans2, LOW);            
                 if (Gear2 == 1) { digitalWrite(GearTrans2, LOW); digitalWrite(GearPos2, HIGH); }  else digitalWrite(GearPos2, LOW);                
                 if (Gear3 > 0 && Gear3 < 1)   digitalWrite(GearTrans3, HIGH); else digitalWrite(GearTrans3, LOW);            
                 if (Gear3 == 1) { digitalWrite(GearTrans3, LOW); digitalWrite(GearPos3, HIGH); }  else digitalWrite(GearPos3, LOW); 

    }       

 }
