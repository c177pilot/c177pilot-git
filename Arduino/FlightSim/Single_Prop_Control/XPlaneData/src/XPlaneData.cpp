/*
	XPlaneData - the library for exchanging DATA with X-Plane by UDP. 
	The latest version of the library, circuts and code samples at:
	http://svglobe.com/arduino/index.html
	Vlad Sychev 2014
*/

#include "XPlaneData.h"

  XPlaneData::XPlaneData ( int XPR, int XPW, int IPx[5], byte mc[6]) 
  {
  XP_R = XPR;
  XP_W = XPW;  
  ipx[0] = IPx[0]; ipx[1] = IPx[1]; ipx[2] = IPx[2]; ipx[3] = IPx[3];
  ip[0] = IPx[0]; ip[1] = IPx[1]; ip[2] = IPx[2];  ip[3] = IPx[4]; 
	for (int i=0; i<6; i++) mac_ad[i] = mc[i];
//	for (int i=0; i<9; i++)	Ext_1[i] = 255;     // for external arduino
  }
  void XPlaneData::begin() {
		
  if (ip[3] == 0) Ethernet.begin(mac_ad);            // start the Ethernet connection:DHCP 
  else Ethernet.begin(mac_ad, ip);       	     // start the Ethernet connection 
  Udp.begin(XP_R);  
	state = 0;
	}

  void XPlaneData::Pins_In(int first, int last) {
for (int i = first; i <= last; i++)  {
	 if (i != 4 && i != 10 && i != 20 && i != 21 && i<50)  pinMode(i, INPUT_PULLUP); 
	 if (i == 13)  pinMode(i, INPUT); 	}
}
/*
  void XPlaneData::SetData_Out(int D_out[3]) {
    char outs[] = "network/dataout/data_to_internet[000]";
	outs[33] = D_out[0]; outs[34] = D_out[1]; outs[35] = D_out[2];
	dref_in(outs, 1);
}
*/
  void XPlaneData::Pins_Out(int first, int last) {
for (int i = first; i < last+1; i++)  {
	 if (i != 4 && i != 10 && i != 20 && i != 21 && i<50)  pinMode(i, OUTPUT); }
}

  float XPlaneData::Out_ini(int grp, int idx) {  // Outputs initialization
    while (udpin() == 0) (udpin());	 return Dget(grp, idx); 
  }

boolean XPlaneData::udpin() {               //read UDP packet		                    
 packetSize = Udp.parsePacket();       
 if (Udp.available())   {             
     char DATA_buf[packetSize];             // Define Data buffer
     Udp.read ( DATA_buf, packetSize );     // Reads received UDP data and store in DATA_buf 
    Data = DATA_buf; return 1;
   }  return 0; }


float XPlaneData::Dget(int group, int param) {  // get float value of parameter (group number and parameter index)
   for (int i = 5; i < packetSize; i += 36)  
        if (Data[i] == group) {
            for (int n=0; n<4; n++) { bf.bytevalue[n] = Data[i+param*4+n]; } 
               return bf.floatvalue;   
        }    }

void XPlaneData::Bus128(int pin1, int pin2, boolean inv) {
byte  cnt = 0;  
for (int j = 0; j < 16; j++ ) {
for (int k = 0; k < 4; k++ )  digitalWrite(pin2+3-k, bitRead (cnt, k)); 
for (int i = 0; i < 8; i++) bitWrite (Bus_in[j], i, digitalRead(i+pin1)^inv);
cnt += 1;         } 
}

//========================= read inputs ----------------


int XPlaneData::Bus_read(int in) {        //
		int n = in/100; in = in-(100*n); n=n-1; 
		boolean rd = bitRead(Bus_in[n], in);	
		if (rd != bitRead(Bus_mem[n], in))  {  bitWrite (Bus_mem[n], in, rd);  return rd; }   // return pin  changed 
			if (rd) return 2; else return 3; }

boolean XPlaneData::Pin_status(int pin) {
		if (pin<100) return !digitalRead(pin); 
		else { int n = pin/100; pin = pin-(100*n);
		return bitRead(Bus_in[n-1], pin); } 
}

int XPlaneData::D_read(int in) {        //read one input (pin #), fix in mem, return status
	if (in>100) {  return Bus_read(in);}   			//-- if BUS
			boolean rd = !digitalRead(in);	
		if (rd != In_mem[in])  {  In_mem[in] = rd;  return rd; }   // return pin  changed 
			if (rd) return 2; else return 3; }

int XPlaneData::E_read(int e1, int e2) {  		  //-- read 2 digital inputs(for Encoders)
	if (e1>100) {  return E_read128(e1, e2); }  // -- if BUS
     boolean rd = !digitalRead(e1); boolean dr = !digitalRead(e1+1); 
		if (e2 == 2) { 
			if (dr != In_mem[e1+1]) {In_mem[e1+1] = dr;  if (rd == dr)   return 1; else return -1; }
			if (rd != In_mem[e1]) { In_mem[e1] = rd; if (rd != dr)   return 1; else return -1; }
			}
		else {
 			if (rd != In_mem[e1]) { 
			 In_mem[e1] = rd;  
				if (e2 == 0) {if (rd != dr)   return 1; else return -1; }
				if (e2 == 1) {if (!rd && !dr) return 1; else if (!rd &&  dr) return -1; }
							}   }
  				return 0;  } 

int XPlaneData::E_read128(int in, int e2) {  		  //-- read 2 digital inputs(for Encoders)
		int n = in/100; in = in-(100*n); n=n-1; 
		boolean rd = bitRead(Bus_in[n], in);	
		boolean dr = bitRead(Bus_in[n], in+1);
		if (e2 == 2) { 
if (dr != bitRead(Bus_mem[n], in+1)) {bitWrite (Bus_mem[n], in+1, dr);  if (rd == dr)   return 1; else return -1; }
if (rd != bitRead(Bus_mem[n], in)) { bitWrite (Bus_mem[n], in, rd) ; if (rd != dr)   return 1; else return -1; }
			}
		else {
 			if (rd != bitRead(Bus_mem[n], in)) { 
			 bitWrite (Bus_mem[n], in, rd);  
				if (e2 == 0) {if (rd != dr)   return 1; else return -1; }
				if (e2 == 1) {if (!rd && !dr) return 1; else if (!rd &&  dr) return -1; }
							}   }
  				return 0;  } 

 
boolean XPlaneData::A_Read(int in, float numb) {     // Read analog, return 1 if changed, fix A_Range
		numb = 1000/numb;  A_range = round(analogRead(in)/numb);
 		if (A_pos[in] != A_range) { A_pos[in] = A_range;  return 1; }
		return 0; } 

//=======================Actuators== analog ==============================================

void XPlaneData::Analog_Input(int in, char dref[], long v1, long v2, float pre) {	// --A in mapping to values 
		if (A_Read(in, pre)) { //A_range = A_range*100;
		float val = map (A_range, 0, pre, v1*1000, v2*1000);
			val = val/1024;	dref_in(dref, val);	}
			 }

boolean XPlaneData::RotarySwitch(int in, int numb) {     // multi-button analog
		numb = 1000/(numb-1);  numb = round(analogRead(in)/float(numb));   A_range = numb+1;
 		if (A_pos[in] != numb) {  A_pos[in] = numb; state = 1; active=in; return 1; } 
		else { state = 0;   return 0; } }   


boolean XPlaneData::ButtonArray(int in, int numb) {         // -- init button array
		if  (A_Read(in, float(numb))) state = 1; else state = 2;    
		 if (A_range ==0) state = 0; 
			if (state == 1) return 1; else return 0; }


//================================ Actuators ======================


void XPlaneData::Button_Cmd(int in, char* D_com) {       			    //button "toggle", button "set" 
	if((A_range == in && state ==1 ) || (D_read(in) == 1 && state !=1)) 
		{ cmd_in(D_com); delay(2); }
	
											}
void XPlaneData::Button_Inc(int in, char* D_com) {      	    //button "Inc/Dec"+cont  
		if (state == 0) {	int s = D_read(in);
			if (s == 1)  { cmd_in(D_com); d_time = millis(); delay(5); } 	
			if (s == 2) { if (millis() - d_time > 800) { cmd_in(D_com); delay(15); } }
		 } 
		else if(A_range == in) {
			if (state ==1) {cmd_in(D_com); a_time = millis();  delay(15); }
			if (state ==2) { if (millis()-a_time > 800) { cmd_in(D_com); delay(15); }}
				 state = 0; }
	}
void XPlaneData::Button_Rep(int in, char* D_com) {           	 						// repeat button, command   
		if ((D_read(in) == 1 && state ==0) || (A_range == in && state >0)) { 
 		 		cmd_in(D_com); delay(5); if (state==0 && in<100) In_mem[in] = !In_mem[in];  
										else if (in>100){ int n = in/100; in = in-(100*n); n=n-1; 
						 bitWrite (Bus_mem[n], in, 0);}
						 else { A_pos[active] = -1; state=0; } }
			}

void XPlaneData::Button_RDf(int in, char* Dref, float Val) {        	    // repeat button, dataref value
		if((A_range == in && state >0 ) || (D_read(in) == 1 && state ==0))
			{ dref_in(Dref, Val); delay(5); 
			if (state==0) In_mem[in] = !In_mem[in]; else { A_pos[active] = -1; state =0; } }
					}

boolean XPlaneData::Set_Value(int in, char* Dref, float Val) {         //button set dref value  
		if((A_range == in && state ==1 ) || (D_read(in) == 1 && state ==0))
			  { dref_in(Dref, Val); delay(5);  state =0;  return 1; } 
					return 0; }

boolean XPlaneData::Set_Val_R(int in, char* Dref, float Val) {         //button set dref value  
		 if(A_range == in && state>0) {
			if (state ==1) {dref_in(Dref, Val); a_time = millis(); state = 0; delay(15); return 1;  }
			if (state ==2) { if (millis()-a_time > 800) { dref_in(Dref, Val); state = 0; delay(15); return 1; }}
				 } 
		else if (state == 0) {	int s = D_read(in);
 			if (s == 1) {  dref_in(Dref, Val);  d_time = millis();  delay(2);  return 1; } 
			if (s == 2)  { if (millis() - d_time > 800) { dref_in(Dref, Val); delay(15);  return 1; } }  
					}  
	return 0;
	}

void XPlaneData::Switch_Com(int in, char D_com1[], char D_com2[]) {   // switch toogle 2 commands  
		int s = D_read(in); 
		if (s == 1) cmd_in(D_com1);
 		else if (s == 0) cmd_in(D_com2); } 


void XPlaneData::Switch_Dref(int in, char* Dref, float On, float Off) {  // switch toogle dataref btw two values
		int s = D_read(in);
			if (s == 1)  dref_in(Dref,  On); 			// switch dref with custom value "on" 
			else if (s == 0) dref_in(Dref,  Off); }			// switch dref with custom value "off"
								
//===================== Encoders =======================================

void XPlaneData::Encoder_Com(int in, char D_com1[], char D_com2[], int type) { 
 			 type = (E_read(in, type)); 
  	  if (type !=0) {
				if (type == 1) cmd_in(D_com1); 
				else  cmd_in(D_com2);  }
				}

void XPlaneData::Encoder_2xCom(int in, int btn, char Com1[], char Com2[], char Com3[], char Com4[], int type)
		{ type = (E_read(in, type));
  	  if (type !=0) { btn = Pin_status(btn);
	 	 if (type == 1) { if (btn) cmd_in(Com1); else cmd_in(Com3); }	  
				else  { if (btn) cmd_in(Com2); else cmd_in(Com4); }	     	  	
		} 	}

float XPlaneData::Encoder_Dref(int in, char* Dref, float val,  float inc, int type)  {    // encoder dref 
 			inc = E_read(in, type)*inc;
  	  if (inc !=0) { val += inc;                   
			dref_in(Dref, val); }
						return val;	 } 

 // encoder w/button dref 
float XPlaneData::Encoder_2xDref(int in, int btn, char* Dref, float val,  float inc1, float inc2, int type) 
 			{  type = E_read(in, type);
  	  if (type !=0) { inc1 = inc1*type; inc2 = inc2*type; 
				 if ( Pin_status(btn)) val += inc1;                     
    			 else  val += inc2; 
  						dref_in(Dref, val);  
							 } return val;	 	}
//------------------------------------------------ Misc

void XPlaneData::Button_Key(int in, char key[]) {        	    // 
		if (state == 0) {	int s = D_read(in);
			if (s == 1)  { key_in(key); d_time = millis(); delay(5); return;} 	
			if (s == 2) { if (millis() - d_time > 800) { key_in(key); delay(15); } }
		 } 
		else if(A_range == in) {
			if (state ==1) {key_in(key); a_time = millis();  delay(15); }
			if (state ==2) { if (millis()-a_time > 800) { key_in(key); delay(15); }}
			 state = 0; 	} }

void XPlaneData::Button_Chr(int in, int key) {        	    // 
		if (state == 0) {	int s = D_read(in);
			if (s == 1)  { char_in(key); d_time = millis(); delay(15); return;} 	
			if (s == 2) { if (millis() - d_time > 800) { char_in(key); delay(15); } }
		 } 
		else if(A_range == in) {
			if (state ==1) {char_in(key); a_time = millis();  delay(15); }
			if (state ==2) { if (millis()-a_time > 800) { char_in(key); delay(15); }}
				state = 0; } }

void XPlaneData::Button_Mnu(int in, char* mnum) {        	    // 
	 if((A_range == in && state ==1 ) || (D_read(in) == 1 && state ==0)) menu_in(mnum) ; }

//===================================================

void XPlaneData::cmd_in(char Data_com[]) {        // send Command 
	Udp.beginPacket(ipx, XP_W); 
	Udp.write(COM) ;  
	Udp.write(Data_com) ;// udpend(); 
	Udp.endPacket(); 
	} 
void XPlaneData::dref_in(char dref[], float Data) {      // Send Dataref ( dataref and value) 
  int n = 0;
   while (dref[n]) { DataRef[n] = dref[n]; n++;}         
         for (int i = n; i < 496; i++) DataRef[i] = char(32);
	Udp.beginPacket(ipx, XP_W);

    Udp.write(DREF);
    bf.floatvalue = Data;
         for (int i=0; i<4; i++)   Udp.write(bf.bytevalue[i]); 
    Udp.write("sim/");
    Udp.write(DataRef) ; 
    Udp.endPacket(); 
    }   
void XPlaneData::key_in(char key_ch[]) {          // Send Char (decimal number of char combination)
    Udp.beginPacket(ipx, XP_W); 
    Udp.write(KEY) ; Udp.write(key_ch) ;                
    Udp.endPacket(); }  

void XPlaneData::char_in(int key) {          // Send Char (decimal number of char combination)
    Udp.beginPacket(ipx, XP_W); 
    Udp.write(KEY) ; Udp.write(key) ;                
    Udp.endPacket(); }  

 void XPlaneData::menu_in(char* mnum)    {         // Menu item
    Udp.beginPacket(ipx, XP_W); 
    Udp.write(MENU) ;  Udp.write(mnum) ;                
    Udp.endPacket(); } 

//==============================================================

/*

//===== Receiving data from Arduino Mega (slave #1,2) ===

int XPlaneData::get_slave(char type[], int row, int id) { 
 Wire.requestFrom(id, 9, true);     // request #2 Arduino Mega (9 bytes)
 if (Wire.available()) {
  for (int i = 0; i < 9; i++)  Ext_dat[i] = Wire.read();     // receive 8 bytes, write to byte array    
    if (row == 1) row = 49;
	else if (row == 2) row = 121;
for (int i = 0; i < 9; i++) {
     Change = Ext_dat[i] ^ Ext_1[i]; // check if 8 bytes are non equal
     if (Change > 0) 
		{ int k = 0; 
			while ( Change > 0) { Change = Change >> 1; k++; }   
       act = !bitRead (Ext_dat[i], k-1);
        if (i < 8) dir = !bitRead (Ext_dat[i], k); 
        int num = (i*8) + row + k;       // pin from #50 to #103 (exluding 70-71, 104,105)
			a_state = num;						// + #106-121 (ANALOG AS DIGITAL) - total 114 digital inputs
       Ext_1[i] = Ext_dat[i]; 
			return num;
	     }
 	  }
 	} return -1;
  }
*/
        
