#include "ADCSWITCH.h"
#include "ADC.h"

/*
  ADCSwitch  
  created 31-December-2015 V 0.1
  By Chris Rose

  Updated 5/15/2022 for changes made to the Teensyduino ADC.h library.
  See: https://forum.pjrc.com/threads/25532-ADC-library-update-now-with-support-for-Teensy-3-1
*/

adcSwitch::adcSwitch(ADC *adc, int pin, int nthrow) {
  m_pin = pin;
  m_throw = nthrow;
  m_adc = adc;
  m_adcMax = adc -> adc0 -> getMaxValue();
}  


bool adcSwitch::update() {
	this->value();
	return m_isChanged;
}

// Press each button in turn and note the values returned to 
// Serial monitor
int adcSwitch::value() {
  // read the analog input into a variable:
  int adc_val = m_adc->analogRead(m_pin);
  m_adcMax = m_adc -> adc0 -> getMaxValue();
  int value = (int)(((float)( adc_val*(m_throw-1) )/m_adcMax)+0.5);
  if (m_lastValue != value) {
	m_isChanged = true;
	m_lastValue = value;
	} else {
	m_isChanged = false;
	}
  return value;
}
