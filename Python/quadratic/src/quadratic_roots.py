import cmath
import math

a = float(input("Enter the coefficient of a: "))
b = float(input("Enter the coefficient of b: "))
c = float(input("Enter the coefficient of c: "))

d = math.pow(b,2)-4*a*c # b^2 - 4ac
droot = cmath.sqrt(d)   #sqrt(b^2 - 4*a*c)

x1 = (-b+droot)/(2*a)
x2 = (-b-droot)/(2*a)
print ("This equation has two solutions: {} or {}".format(x1,x2))
