/*

Sample code for 16-Port, 128 inputs expansion board.
Download  XPlaneData Library on the site.

I/O interface for X-Plane Cockpit Simulator

(Do not leave any "Serial.print()" strings in the code - 
they take a lot of processing time and may affect the encoders)

Vlad Sychev, 2012-2014
http://svglobe.com/arduino/ports.html
 */
 
#include <SPI.h>         
#include <Ethernet.h>
#include <Wire.h>
#include <XPlaneData.h>         // -- include XPlaneData library

//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8, 0};                     // X-Plane IP address ( ! set your !)
//IPAddress ip;                                   // Arduino IP address (Auto)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)



//=================  setup  ======================================
void setup()  { 

   XP.begin();      //==! init XPlaneData 

XP.Pins_In (22, 29);    // Define pins for inputs  ( 8 pins for bus) - first and last
XP.Pins_Out (30, 33);   // Define pins for outputs (4 pins for address) - first and last
   } 
           
//========= Setup End ===========

//------------------------  Program start -----------------------
void loop() 
{
   

XP.Bus128(22, 30, 0);  // --- read data from bus

//-------- Port #1 (0-7) ------- switches

XP.Switch_Com (100, "systems/avionics_on", "systems/avionics_off");                 // Master Avionics switch     
XP.Switch_Com (101, "lights/nav_lights_on", "lights/nav_lights_off");               // NAV light  switch  
XP.Switch_Com (102, "lights/beacon_lights_on", "lights/beacon_lights_off");         // Beacon Light switch   
XP.Switch_Com (103, "lights/strobe_lights_on", "lights/strobe_lights_off");         // Strobe Light switch    
XP.Switch_Com (104, "lights/taxi_lights_on", "lights/taxi_lights_off");             // Taxi  light switch  
XP.Switch_Com (105, "electrical/battery_1_on", "electrical/battery_1_off");         // BATTERY switch    
XP.Switch_Com (106, "electrical/generator_2_on", "electrical/generator_2_off");     // R Alt switch     
XP.Switch_Com (107, "electrical/generator_1_on", "electrical/generator_1_off");     // L Alt  switch  

//-------- Port #2 (200-207) ------- switches

XP.Switch_Com (200, "ice/inlet_heat0_on", "ice/inlet_heat0_off");   // Fuel Vent De-Icing   
XP.Switch_Com (201, "ice/prop_heat0_on", "ice/prop_heat0_off");    // Prop Heat switch               
XP.Switch_Com (202, "ice/window_heat_on", "ice/window_heat_off");   // WSHLD  switch   
XP.Switch_Com (203, "ice/pitot_heat0_on", "ice/pitot_heat0_off");   // Pitot Heat switch
XP.Switch_Com (204, "ice/AOA_heat0_on", "ice/AOA_heat0_off");       // Stall Wrn Heating  
XP.Switch_Com (205, "ice/wing_heat0_on", "ice/wing_heat0_off");    // AUTO Surface De-Icing sw                          
XP.Switch_Com (206, "fuel/fuel_pump_1_on", "fuel/fuel_pump_1_off");  // FUEL Pump L  
XP.Switch_Com (207, "fuel/fuel_pump_2_on", "fuel/fuel_pump_2_off");  // FUEL PUMP R   


//-------- Port #3 (300-307) ------- Buttons 

XP.Button_Cmd (300, "flight_controls/landing_gear_down");               // GEAR DN    
XP.Button_Cmd (301, "flight_controls/landing_gear_up");                 // GEAR UP  
XP.Button_Cmd (302, "radios/com1_standy_flip");      //-------------- -- Com1/Com1-stby flip
XP.Button_Cmd (303, "radios/nav1_standy_flip");      //-------------- -- NAV1/NAV1-stby flip
XP.Button_Cmd (304, "radios/com2_standy_flip");      //-------------- -- Com2/Com2-stby flip
XP.Button_Cmd (305, "radios/nav2_standy_flip");      //-------------- -- NAV2/NAV2-stby flip
XP.Button_Cmd (306, "radios/adf1_standy_flip");                  //--- Change ADF1 stdby Freq 
XP.Button_Cmd (307, "radios/adf2_standy_flip");                  //--- Change ADF2 stdby Freq 
    
//-------- Port #4 (400-407) ------- Buttons - audio panel (XPlane 10)


XP.Button_Cmd (400, "audio_panel/monitor_audio_com1");        //  A_Com1
XP.Button_Cmd (401, "audio_panel/monitor_audio_com2");        //  A_Com2    
XP.Button_Cmd (402, "audio_panel/monitor_audio_nav1");        //  A_Nav1
XP.Button_Cmd (403, "audio_panel/monitor_audio_nav2");        //  A_Nav2     
XP.Button_Cmd (404, "audio_panel/monitor_audio_adf1");        //  A_Adf 1
XP.Button_Cmd (405, "audio_panel/monitor_audio_adf2");        //  A_Adf 1
XP.Button_Cmd (406, "audio_panel/monitor_audio_dme");         //  A_DME  
XP.Button_Cmd (407, "audio_panel/monitor_audio_mkr");         //  A_Mkr 


//-------- Port #5 (500-507) ------- Test buttons / clear buttons

XP.Button_Rep (500, "annunciator/test_all_annunciators");          // Test Button - all ann
XP.Button_Rep (501, "autopilot/test_auto_annunciators");          // Test autopilot ann
XP.Button_Rep (502, "annunciator/test_fire_1_annun");             // test fire 1
XP.Button_Rep (503, "annunciator/test_fire_2_annun");             // test fire 1
XP.Button_Rep (504, "annunciator/test_stall");                    // stall warn test
XP.Button_Cmd (505, "annunciator/clear_master_caution");         //   Clear master caution.
XP.Button_Cmd (506, "annunciator/clear_master_warning");         //   Clear master Warning.
XP.Button_Cmd (507, "annunciator/gear_warning_mute");             //  Clear Gear Warning .             


//-------- Port #6 (600-607) ------- 6-position transponder switch /Ident command 

XP.Button_Cmd (600, "transponder/transponder_ident");           //------IDENT  ----                     
XP.Button_Cmd (601, "transponder/transponder_standby");         //------STBY--- 
XP.Button_Cmd (602, "transponder/transponder_alt");             // - ALT mode
XP.Button_Cmd (603, "transponder/transponder_test");            // - TEST mode
XP.Button_Cmd (604, "transponder/transponder_on");              // - ON mode
XP.Button_Cmd (605, "transponder/transponder_ground");          // - GND mode
XP.Button_Cmd (606, "transponder/transponder_ident");          //------Transponder IDENT button ----  
//  607

//-------- Port #7 (700-707) ------- Encoders, single (4-knob Transponder) 

XP.Encoder_Com(700, "transponder/transponder_ones_up", "transponder/transponder_ones_down", 0);            // Transponder 1
XP.Encoder_Com(702, "transponder/transponder_tens_up", "transponder/transponder_tens_down", 0);            // Transponder 10   
XP.Encoder_Com(704, "transponder/transponder_hundreds_up", "transponder/transponder_hundreds_down", 0);    // Transponder 100
XP.Encoder_Com(706, "transponder/transponder_thousands_up", "transponder/transponder_thousands_down", 0);  // Transponder 1000


//-------- Port #8 (800-807) ------- Encoders, single ( 3-knob ADF, Barometer) 

XP.Encoder_Com(800, "radios/stby_adf1_ones_up", "radios/stby_adf1_ones_down", 0);          // ADF1 Freq 1
XP.Encoder_Com(802, "radios/stby_adf1_tens_up", "radios/stby_adf1_tens_down", 0);          // ADF1 Freq 10
XP.Encoder_Com(804, "radios/stby_adf1_hundreds_up", "radios/stby_adf1_hundreds_down", 0);  // ADF1 Freq 100

XP.Encoder_Com(806, "instruments/barometer_up", "instruments/barometer_down", 0); // Barometer, alt 


//-------- Port #9 (900-9807) ------- Encoders, double, with button (4 commands)

XP.Encoder_2xCom(900, 3, "radios/stby_com1_coarse_down", "radios/stby_com1_coarse_up", 
"radios/stby_com1_fine_down","radios/stby_com1_fine_up", 0); 

XP.Encoder_2xCom(902, 3, "radios/stby_com2_coarse_down", "radios/stby_com2_coarse_up", 
"radios/stby_com2_fine_down", "radios/stby_com2_fine_up", 0); 

XP.Encoder_2xCom(904, 3, "radios/stby_com1_coarse_down", "radios/stby_com1_coarse_up", 
"radios/stby_com1_fine_down","radios/stby_com1_fine_up", 0); 

XP.Encoder_2xCom(906, 3, "radios/stby_com2_coarse_down", "radios/stby_com2_coarse_up", 
"radios/stby_com2_fine_down", "radios/stby_com2_fine_up", 0); 



//-------- Port #10 (1000-1007) ------- 

/*

    You casn use up to 16 ports 

*/

//-------- Port #15 (1500-1507) -------

//-------- Port #16 (1600-1607) -------
 
 // ---- End of 128-bit bus reading
 
 //========== Here you can use another free Arduino Mega pins for inputs/outputs :
 
 
     XP.Button_Inc(2, "radios/obs_HSI_up");                   // OBS HSI +  increment button          
     XP.Button_Inc(3, "radios/obs_HSI_down"); 
 
            //   and so on....
   

               

 }   //-----End 

  
