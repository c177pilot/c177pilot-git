#define ENCODER_DO_NOT_USE_INTERRUPTS //necessary if the dedicated interrupts are not being used.
#include <Encoder.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

/*Change Log
*/

LiquidCrystal_I2C lcd(0x27,16,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

long com_enc_coarse, com_enc_fine, nav_enc_coarse, nav_enc_fine;
const int ENCCOMCOARSEPIN_A=20;
const int ENCCOMCOARSEPIN_B=21;
const int ENCNAVCOARSEPIN_A=16;
const int ENCNAVCOARSEPIN_B=17;

const int ENCCOMFINEPIN_A=5;
const int ENCCOMFINEPIN_B=6;
const int ENCNAVFINEPIN_A=7;
const int ENCNAVFINEPIN_B=8;

Encoder EncNavCoarse = Encoder(ENCNAVCOARSEPIN_A,ENCNAVCOARSEPIN_B);// Rotary Encoder A channel pin and B channel pin
Encoder EncNavFine = Encoder(ENCNAVFINEPIN_A,ENCNAVFINEPIN_B);
Encoder EncComCoarse = Encoder(ENCCOMCOARSEPIN_A,ENCCOMCOARSEPIN_B);
Encoder EncComFine = Encoder(ENCCOMFINEPIN_A,ENCCOMFINEPIN_B);

// setup runs once, when Teensy boots.
void setup() {
 
  lcd.init();                      // initialize the lcd 
  lcd.clear();
  lcd.backlight();
 
  Serial.begin(9600);

//  pinMode(ENCNAVCOARSEPIN_A, INPUT_PULLUP); //use 4.7k external resistors on this encoder as experiment
//  pinMode(ENCNAVCOARSEPIN_B, INPUT_PULLUP);
//  pinMode(ENCNAVFINEPIN_A, INPUT_PULLUP);
//  pinMode(ENCNAVFINEPIN_B, INPUT_PULLUP);
  pinMode(ENCCOMCOARSEPIN_A, INPUT_PULLUP);
  pinMode(ENCCOMCOARSEPIN_B, INPUT_PULLUP);
  pinMode(ENCCOMFINEPIN_A, INPUT_PULLUP);
  pinMode(ENCCOMFINEPIN_B, INPUT_PULLUP);  

} //end of setup

void loop() {

  char buf[3];
  // normally the first step in loop() should update from X-Plane  
  //FlightSim.update(); //cache X-plane variables
  //refreshLCD();  

  // read the rotary encoders
  com_enc_fine = EncComFine.read() / 2;
  com_enc_coarse = EncComCoarse.read() / 2;
  nav_enc_fine = EncNavFine.read() / 2;
  nav_enc_coarse = EncNavCoarse.read() / 2;

  itoa(com_enc_coarse,buf,10);
  lcd.setCursor(0,0);
  lcd.print(buf);
  
  itoa(com_enc_fine,buf,10);
  lcd.setCursor(0,1);
  lcd.print(buf);
  
  itoa(nav_enc_coarse,buf,10);
  lcd.setCursor(0,2);
  lcd.print(buf);
  
  itoa(nav_enc_fine,buf,10);
  lcd.setCursor(0,3);
  lcd.print(buf);
  
  delay(5);
}


