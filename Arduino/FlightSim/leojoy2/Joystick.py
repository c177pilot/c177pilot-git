# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pygame
import sys
import time

pygame.joystick.init()

print "Joystick count: "
joycount = pygame.joystick.get_count()
print joycount

if (joycount > 0):
    _joystick = pygame.joystick.Joystick(0)
    _joystick.init()
    print "Joystick Init: ", _joystick.get_init()
    print "Joystick ID: ", _joystick.get_id()
    print "Joystick Name: ",_joystick.get_name()
    print "Joystick NumAxes: ",_joystick.get_numaxes()
    print "Joystick NumBalls: ",_joystick.get_numballs()
    print "Joystick NumButtons: ",_joystick.get_numbuttons()
    print "Joystick NumHats: ",_joystick.get_numhats()
    print "Joystick Axis(0): ",_joystick.get_axis(0)
else:
    print "No Joysticks found"
