# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 16:33:12 2016

@author: crose
"""
from scipy import signal
import matplotlib.pyplot as plt
import numpy as np

t = np.linspace(-1, 1, 2 * 100, endpoint=False)
isig, qsig, envelope = signal.gausspulse(t, fc=5, retquad=True, retenv=True)
plt.plot(t, isig, t, qsig, t, envelope, '--')