/*

-- 21 encoders on one Arduino-Mega with Ethernet Shield
using the XPlaneData Library

sample code for all encoders
for X-Plane

Vlad, April 2014
http://svglobe.com
 */

//------------------------------------
#include <SPI.h>         
#include <Ethernet.h>
#include <Wire.h>
#include <XPlaneData.h>         // -- include XPlaneData library
//-----------------------------
byte Ard_MAC[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};// MAC-Address of Arduino
int XPR = 49001;                                  // port to listen to get UDP packets
int XPW = 49000;                                  // port to send UDP packets
int IPx[] = {192, 168, 1, 8, 0};                     // X-Plane IP address ( ! set your !)
//IPAddress ip;                                   // Arduino IP address (Auto)

XPlaneData XP (XPR, XPW, IPx, Ard_MAC);   //==!==  Create  XPlaneData object (XP)
//------------------------------------

float VVI;
float ADF; 

int adf; // ADF encoder built-in button status (cyclic  3-digits positions ) 
int trs; // Transponder encoder built-in button status (cyclic  4-digits positions, for  TDR-94/CTL-92 display unit) 

unsigned long time; 
//=================  setup  ==========
void setup()  { 
  
   XP.begin();      //==! init XPlaneData  

  XP.Pins_In (0, 49);   // Define pins for inputs (first, last including)

      }

void loop() 
{
//===========  Reading all  inputs (except 20,21 pins for I2C,  4, 10, 50-53 pins for EShield)=========================== 

// ================= all cycle takes about 0.5 msec === 


// Input #0  - Encoders built-in button"
// Input #1  - ADF cycle button: Cycle "ones-tens-hundreds"
// Input #5  - Transponder cycle button: Cycle "ones-tens-hundreds-thousands"       

                              // Push buttons of all Com/Nav ebcoders  = pin #0
                              // Push button of ADF encoder  = pin #1
                              // Push buttons of Transponder (TDR-94/CTL-92) ebcoder  = pin #5           


 if (XP.D_read(1) == 1) { adf++; if (adf == 3) adf = 0; delay(15);}                             
 if (XP.D_read(5) == 1) { trs++; if (trs == 4) trs = 0; delay(15);}
   
   
XP.Encoder_Com(2, "instruments/barometer_up", "instruments/barometer_down", 0); // Barometer, alt   
ADF = XP.Encoder_Dref(6, "cockpit2/radios/actuators/adf1_card_heading_deg_mag_pilot[0]", ADF, 1, 0);  // ADF1 Card Pilot     

XP.Encoder_Com(8, "radios/obs_HSI_up", "radios/obs_HSI_down", 0);                    // HSI OBS   
XP.Encoder_Com(11, "autopilot/heading_up", "autopilot/heading_down", 0);               // AP Heading (HSI bug)   
XP.Encoder_Com(14, "autopilot/airspeed_up", "autopilot/airspeed_down", 0);                  // AP IAS select   
XP.Encoder_Com(16, "autopilot/altitude_up", "autopilot/altitude_down", 0);                  // AP ALT select
XP.Encoder_Com(18, "autopilot/vertical_speed_up", "autopilot/vertical_speed_down", 0);          // AP VVI select (use comand,step=200) 

VVI = XP.Encoder_Dref(20, "cockpit2/autopilot/vvi_dial_fpm[0]", VVI, 100, 0);                    // AP VVI select (use dstaref, step=100) 
 
XP.Encoder_2xCom(24, 0, "radios/stby_com1_fine_up", "radios/stby_com1_fine_down", "radios/stby_com1_coarse_up", "radios/stby_com1_coarse_down", 0);        // Com1 Freq   
XP.Encoder_2xCom(26, 0, "radios/stby_com2_fine_up", "radios/stby_com2_fine_down", "radios/stby_com2_coarse_up", "radios/stby_com2_coarse_down", 0);         // Com2 Freq
XP.Encoder_2xCom(28, 0, "radios/stby_nav1_fine_up", "radios/stby_nav1_fine_down", "radios/stby_nav1_coarse_up", "radios/stby_nav1_coarse_down", 0);         // Nav1 Freq
XP.Encoder_2xCom(30, 0, "radios/stby_nav2_fine_up", "radios/stby_nav2_fine_down", "radios/stby_nav2_coarse_up", "radios/stby_nav2_coarse_down", 0);         // Nav2 Freq

if(adf==0) XP.Encoder_Com(32, "radios/stby_adf1_ones_up", "radios/stby_adf1_ones_down", 0);                  // ADF Freq 1
if(adf==1) XP.Encoder_Com(32, "radios/stby_adf1_tens_up", "radios/stby_adf1_tens_down", 0);                   // ADF Freq 10
if(adf==2) XP.Encoder_Com(32, "radios/stby_adf1_hundreds_up", "radios/stby_adf1_hundreds_down", 0);    // ADF Freq 100
 
XP.Encoder_Com(34, "radios/obs2_up", "radios/obs2_down", 0);                        // Nav2 OBS    

if(trs==0) XP.Encoder_Com(36, "transponder/transponder_ones_up", "transponder/transponder_ones_down", 0);               // Transponder 1
if(trs==1) XP.Encoder_Com(36, "transponder/transponder_tens_up", "transponder/transponder_tens_down", 0);                // Transponder 10   
if(trs==2) XP.Encoder_Com(36, "transponder/transponder_hundreds_up", "transponder/transponder_hundreds_down", 0);         // Transponder 100
if(trs==3) XP.Encoder_Com(36, "transponder/transponder_thousands_up", "transponder/transponder_thousands_down", 0); // Transponder 1000

XP.Encoder_Com(38, "flight_controls/pitch_trim_up", "flight_controls/pitch_trim_down", 0);        // TRIM Pitch
XP.Encoder_Com(40, "flight_controls/aileron_trim_left", "flight_controls/aileron_trim_right", 0); // TRIM Roll
XP.Encoder_Com(42, "flight_controls/rudder_trim_left", "flight_controls/rudder_trim_right", 0);   // TRIM Yaw
XP.Encoder_Com(44, "instruments/dh_ref_up", "instruments/dh_ref_down", 0);                        // Decision height (Radio-ALT bug)
XP.Encoder_Com(46, "instruments/map_zoom_in", "instruments/map_zoom_out", 0);                     // Zoom EFIS Map
XP.Encoder_Com(48, "instruments/instrument_bright_up", "instruments/instrument_bright_down", 0);   // Instrument Brightness   


}
