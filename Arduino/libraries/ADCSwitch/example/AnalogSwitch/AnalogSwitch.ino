
/*
  Analog input, analog output, serial output

 Reads an analog input pin, maps the result to a range from 0 to 255
 and uses the result to set the pulsewidth modulation (PWM) of an output pin.
 Also prints the results to the serial monitor.

 The circuit:
 * potentiometer connected to analog pin 0.
   Center pin of the potentiometer goes to the analog pin.
   side pins of the potentiometer go to +5V and ground
 * LED connected from digital pin 9 to ground

 created 29 Dec. 2008
 modified 9 Apr 2012
 by Tom Igoe

 This example code is in the public domain.

 */
#define NTAPS 3
#include "ADC.h" 
#include "adcSwitch.h"

// These constants won't change.  They're used to give names
// to the pins used:
const int analogInPin = A1;  // Analog input pin that the potentiometer is attached to

ADC *adc = new ADC(); // adc object
adcSwitch analogSwitch = adcSwitch(adc, analogInPin, NTAPS); //create an analogSwitch object
int sensorValue = 0;        // value read from the pot

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  Serial.println("I'm Alive!!!");

  //setup the Teensy ADC
  adc->adc0->setReference(ADC_REFERENCE::REF_3V3);
  adc->adc0->setResolution(16);
  adc->adc0->setSamplingSpeed(ADC_SAMPLING_SPEED::VERY_LOW_SPEED);
  adc->adc0->setAveraging((2^6)-1);
}

void loop() {
  // read the analog in value:
  sensorValue = adc->analogRead(analogInPin); //get the raw ADC value for comparison to the "switched value"

  // print the results to the serial monitor:
  Serial.print("\t raw sensor = ");
  Serial.print(sensorValue);

  Serial.print("\t adcSwitch Update = ");
  if (analogSwitch.update()) {
    Serial.print(analogSwitch.value()); //this will reset the update() so it must be printed before the call to update()
  } else {
    Serial.print("No change");
  }

  Serial.print("\t adcSwitch = ");
  Serial.println(analogSwitch.value());

  delay(500);
}
