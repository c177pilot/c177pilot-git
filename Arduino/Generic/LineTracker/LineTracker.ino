//Define the input and output pins from the microcontroller
#define motor1Pin 3
#define motor2Pin 9
#define photoCellPin A0
//Analog ranges are from 0 to 255, where 255 is full speed
#define FASTSPEED 255
#define SLOWSPEED 64
#define LIGHTINCREASE 1.5

int lightVoltage = 0;
int firstLightVoltage=0;

void setup() {
  //read the light level at startup
  firstLightVoltage=analogRead(photoCellPin);
}

void loop() {
  
  analogWrite(motor1Pin, SLOWSPEED);  
  analogWrite(motor2Pin, SLOWSPEED);

  lightVoltage=analogRead(photoCellPin);  

  if (lightVoltage > firstLightVoltage*LIGHTINCREASE) {
    analogWrite(motor1Pin, FASTSPEED);
    analogWrite(motor2Pin, SLOWSPEED);
    delay(4000);
    analogWrite(motor1Pin, FASTSPEED);
    analogWrite(motor2Pin, FASTSPEED);
    delay(3000); //wait 3000 milliseconds
  }

  delay(200);              // wait 200 milliseconds

}


